package sg.gov.mpa.msw.prearrivalapplicationservice.model.event;

import java.time.LocalDateTime;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@Valid
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShipCertificateEvent {

	@Size(max = 100, message = "Certificate Type should not exceed 100 characters.")
	private String certificateType;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-09-21 21:20:00")
	private LocalDateTime issueDate;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-09-21 21:20:00")
	private LocalDateTime dueDate;

	@Size(max = 100, message = "Issuing Authority should not exceed 100 characters.")
	private String issuingAuthority;

	@Size(max = 100, message = "Issuing Class should not exceed 100 characters.")
	private String issuingClass;

	@NotNull
	@Size(max = 100, message = "Created By should not exceed 100 characters.")
	private String createdBy;

}
