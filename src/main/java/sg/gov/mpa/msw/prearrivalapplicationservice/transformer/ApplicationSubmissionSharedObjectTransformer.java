package sg.gov.mpa.msw.prearrivalapplicationservice.transformer;

import java.util.Optional;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import sg.gov.mpa.msw.core.interfaces.SharedObjectToEntityTransformer;
import sg.gov.mpa.msw.core.transformer.AbstractCopyPropertiesTransformer;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationSubmission;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationType;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.PreArrivalApplication;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.sharedobject.ApplicationSubmissionSharedObject;
import sg.gov.mpa.msw.prearrivalapplicationservice.repository.ApplicationTypeRepository;
import sg.gov.mpa.msw.prearrivalapplicationservice.repository.PreArrivalApplicationRepository;

@Component
@AllArgsConstructor
public class ApplicationSubmissionSharedObjectTransformer
		extends AbstractCopyPropertiesTransformer<ApplicationSubmissionSharedObject, ApplicationSubmission>
		implements SharedObjectToEntityTransformer<ApplicationSubmissionSharedObject, ApplicationSubmission, Long> {

	PreArrivalApplicationRepository preArrivalApplicationRepository;
	ApplicationTypeRepository applicationTypeRepository;

	@Override
	public ApplicationSubmission apply(ApplicationSubmissionSharedObject orig) {
		ApplicationSubmission applicationSubmission = super.apply(orig);
		Optional<PreArrivalApplication> preArrivalApplication = preArrivalApplicationRepository
				.findById(orig.getPreArrivalApplication());
		preArrivalApplication.ifPresent(applicationSubmission::setPreArrivalApplication);
		Optional<ApplicationType> applicationType = applicationTypeRepository.findById(orig.getApplicationType());
		applicationType.ifPresent(applicationSubmission::setApplicationType);
		return applicationSubmission;
	}

}
