package sg.gov.mpa.msw.prearrivalapplicationservice.model.event;

import java.time.LocalDateTime;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.VesselHealthDeclarationType;

@Data
@Valid
@JsonIgnoreProperties(ignoreUnknown = true)
public class VesselHealthDeclarationEvent {

	@NotNull(message = "Declaration Type is required")
	private VesselHealthDeclarationType vesselHealthDeclarationType;

	@NotNull(message = "Declaration Value is required")
	private Boolean declarationValue;

	@Size(max = 30, message = "Declaration remarks cannot be more than 30 characters.")
	private String declarationRemarks;

	@Size(min = 1, max = 100, message = "Created By is required. Created By should not exceed 100 characters.")
	private String createdBy;
	
	@NotNull(message="Document Id is required")
	private Integer documentId;

	@NotNull(message = "Created On is required")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-09-21 21:20:00")
	private LocalDateTime createdOn;

}
