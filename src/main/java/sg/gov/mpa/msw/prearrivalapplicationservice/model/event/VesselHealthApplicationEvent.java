package sg.gov.mpa.msw.prearrivalapplicationservice.model.event;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import sg.gov.mpa.msw.core.event.MSWKafkaEvent;

@Data
@Valid
@JsonIgnoreProperties(ignoreUnknown = true)
public class VesselHealthApplicationEvent implements MSWKafkaEvent {

	@Size(min = 1, max = 250, message = "MSW Application Reference is required. Msw application reference cannot be more than 250 characters.")
	private String mswApplicationReference;

	@NotNull(message = "Voyage Id is required")
	private Long voyageId;

	@Size(min = 1, max = 100, message = "Applicant Id is required. Applicant id cannot be more than 100 characters.")
	private String applicantId;

	@NotNull(message = "Created On is required")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-09-21 21:20:00")
	private LocalDateTime createdOn;

	@Size(min = 1, max = 100, message = "Created By is required. Created By should not exceed 100 characters.")
	private String createdBy;

	private List<VesselHealthDeclarationEvent> vesselHealthDeclarations = new ArrayList<>();

	private List<VesselHealthDeceasedDetailEvent> vesselHealthDeceasedDetails = new ArrayList<>();

	private List<VesselHealthSicknessOnBoardDetailEvent> vesselHealthSicknessOnBoardDetail = new ArrayList<>();
	
	private VesselHealthRefugeeInformationEvent vesselHealthRefugeeInformation;

	@Override
	public String getRoutingKey() {
		return "";
	}
}
