package sg.gov.mpa.msw.prearrivalapplicationservice.model.event;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import sg.gov.mpa.msw.core.event.MSWKafkaEvent;

@Data
@Valid
@JsonIgnoreProperties(ignoreUnknown = true)
public class PortHealthApplicationSubmittedToAgencyEvent implements MSWKafkaEvent {

	@Size(min = 1, max = 250, message = "MSW Application Reference is required and size should not exceed 250 characters.")
	private String mswReferenceId;

	@Size(max = 100, message = "External Application Reference cannot be more than 100 characters.")
	private String externalReferenceId;

	@Override
	public String getRoutingKey() {
		return "";
	}
}
