package sg.gov.mpa.msw.prearrivalapplicationservice.transformer;

import sg.gov.mpa.msw.core.interfaces.SharedObjectToEntityTransformer;
import sg.gov.mpa.msw.core.transformer.AbstractCopyPropertiesTransformer;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationType;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.sharedobject.ApplicationTypeSharedObject;

public class ApplicationTypeSharedObjectTransformer
		extends AbstractCopyPropertiesTransformer<ApplicationTypeSharedObject, ApplicationType>
		implements SharedObjectToEntityTransformer<ApplicationTypeSharedObject, ApplicationType, String> {

}
