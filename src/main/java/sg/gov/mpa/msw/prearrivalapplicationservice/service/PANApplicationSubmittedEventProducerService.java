package sg.gov.mpa.msw.prearrivalapplicationservice.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import sg.gov.mpa.msw.prearrivalapplicationservice.config.KafkaConfig;
import sg.gov.mpa.msw.prearrivalapplicationservice.exception.EventProducerException;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationSubmission;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationSubmissionStatus;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationTypeName;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.event.PANApplicationSubmittedEvent;

@Service
public class PANApplicationSubmittedEventProducerService implements ApplicationSubmittedEventProducerService {

	@Value("${kafka.topic.name.pans-submission}")
	private String panSubmissionTopic;

	private final KafkaConfig kafkaConfig;

	private final KafkaTemplate<String, PANApplicationSubmittedEvent> panApplicationSubmittedEventKafkaTemplate;

	private final MappingJackson2HttpMessageConverter springJacksonConverter;

	public PANApplicationSubmittedEventProducerService(KafkaConfig kafkaConfig,
			KafkaTemplate<String, PANApplicationSubmittedEvent> panApplicationSubmittedEventKafkaTemplate,
			MappingJackson2HttpMessageConverter springJacksonConverter) {
		this.kafkaConfig = kafkaConfig;
		this.panApplicationSubmittedEventKafkaTemplate = panApplicationSubmittedEventKafkaTemplate;
		this.springJacksonConverter = springJacksonConverter;
	}

	public void sendApplicationSubmittedEvent(final ApplicationSubmission entity) {
		if (entity.getApplicationSubmissionStatus() == ApplicationSubmissionStatus.SUBMITTED) {
			ObjectMapper objectMapper = springJacksonConverter.getObjectMapper();
			PANApplicationSubmittedEvent panApplicationSubmittedEvent = null;
			try {
				panApplicationSubmittedEvent = objectMapper.readValue(entity.getApplicationData(),
						PANApplicationSubmittedEvent.class);
			} catch (IOException e) {
				throw new EventProducerException("Exception while producing PAN event");
			}
			panApplicationSubmittedEventKafkaTemplate.send(kafkaConfig.getTopicPrefix() + panSubmissionTopic,
					panApplicationSubmittedEvent);
		}
	}

	public boolean isApplicable(final ApplicationSubmission entity) {
		return ApplicationTypeName.PAN.name().equals(entity.getApplicationType().getId());
	}

}
