package sg.gov.mpa.msw.prearrivalapplicationservice.model.sharedobject;

import java.time.LocalDateTime;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import sg.gov.mpa.msw.core.interfaces.BaseSharedObject;

@Data
@Valid
@ApiModel
public class ApplicationTypeSharedObject implements BaseSharedObject<String> {

	private String id;

	private String description;

	@NotNull
	private Long lockVersion;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-09-21 21:20:00")
	private LocalDateTime createdOn;

	@Size(max = 100, message = "Created By cannot be more than 100 characters.")
	private String createdBy;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-09-21 21:20:00")
	private LocalDateTime lastUpdatedOn;

	@Size(max = 100, message = "Last Updated By cannot be more than 100 characters.")
	private String lastUpdatedBy;

}
