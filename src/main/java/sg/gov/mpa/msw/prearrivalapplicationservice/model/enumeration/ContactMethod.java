package sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration;

public enum ContactMethod {
	EMAIL, SMS;
}
