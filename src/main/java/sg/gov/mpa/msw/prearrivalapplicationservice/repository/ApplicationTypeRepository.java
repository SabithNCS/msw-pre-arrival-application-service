package sg.gov.mpa.msw.prearrivalapplicationservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationType;

public interface ApplicationTypeRepository
		extends JpaRepository<ApplicationType, String>, JpaSpecificationExecutor<ApplicationType> {

}
