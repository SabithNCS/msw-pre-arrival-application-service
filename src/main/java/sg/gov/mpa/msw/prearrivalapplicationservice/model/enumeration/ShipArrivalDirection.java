package sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration;

public enum ShipArrivalDirection {
	NORTH, SOUTH
}
