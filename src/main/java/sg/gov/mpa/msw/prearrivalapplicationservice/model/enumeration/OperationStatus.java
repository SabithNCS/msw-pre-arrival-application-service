package sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration;

public enum OperationStatus {
	DISCHARGE, LOADING, TRANSIT;
}
