package sg.gov.mpa.msw.prearrivalapplicationservice.transformer;

import org.springframework.stereotype.Component;

import sg.gov.mpa.msw.core.interfaces.EntityToSharedObjectTransformer;
import sg.gov.mpa.msw.core.transformer.AbstractCopyPropertiesTransformer;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationSubmission;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.sharedobject.ApplicationSubmissionSharedObject;

@Component
public class ApplicationSubmissionEntityTransformer
		extends AbstractCopyPropertiesTransformer<ApplicationSubmission, ApplicationSubmissionSharedObject>
		implements EntityToSharedObjectTransformer<ApplicationSubmission, ApplicationSubmissionSharedObject, Long> {
	
	@Override
	public ApplicationSubmissionSharedObject apply(ApplicationSubmission orig) {
		ApplicationSubmissionSharedObject  applicationSubmissionSharedObject = super.apply(orig);
		applicationSubmissionSharedObject.setApplicationType(orig.getApplicationType().getId());
		applicationSubmissionSharedObject.setPreArrivalApplication(orig.getPreArrivalApplication().getId());
		return applicationSubmissionSharedObject;
	}
}
