package sg.gov.mpa.msw.prearrivalapplicationservice.validator;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import lombok.AllArgsConstructor;
import sg.gov.mpa.msw.core.command.ChangeEntityCommand;
import sg.gov.mpa.msw.core.command.validator.CommandValidator;
import sg.gov.mpa.msw.prearrivalapplicationservice.exception.PanValidationException;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationSubmission;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationSubmissionStatus;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationTypeName;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.event.PANApplicationSubmittedEvent;
import sg.gov.mpa.msw.prearrivalapplicationservice.repository.ApplicationSubmissionRepository;

@SuppressWarnings("rawtypes")
@Component
@AllArgsConstructor
public class PanApplicationSubmissionValidator implements CommandValidator<ChangeEntityCommand> {

	ApplicationSubmissionRepository applicationSubmissionRepository;

	Validator validator;

	@Override
	public boolean isApplicable(ChangeEntityCommand command) {
		return ((command.getChangeType() == ChangeEntityCommand.ChangeType.UPDATE)
				&& (command.getEntity() instanceof ApplicationSubmission) && (ApplicationTypeName.PAN.name()
						.equals(((ApplicationSubmission) command.getEntity()).getApplicationType().getId())));
	}

	@Override
	public void validate(ChangeEntityCommand command) {
		ApplicationSubmission applicationSubmissionEntity = (ApplicationSubmission) command.getEntity();
		String panApplicationData = applicationSubmissionEntity.getApplicationData();
		ApplicationSubmissionStatus applicationStatus = applicationSubmissionEntity.getApplicationSubmissionStatus();
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.registerModule(new JavaTimeModule());
		try {
			JsonNode panJsonObj = objectMapper.readTree(panApplicationData);
			if ((panJsonObj == null || (panJsonObj != null && panJsonObj.size() < 1))
					&& (applicationStatus == ApplicationSubmissionStatus.SUBMITTED
							|| applicationStatus == ApplicationSubmissionStatus.DRAFT))
				throw new PanValidationException("PAN Application must have atleast one field");

			if (applicationStatus == ApplicationSubmissionStatus.SUBMITTED) {
				PANApplicationSubmittedEvent panAppObjMapper = objectMapper.readValue(panApplicationData,
						PANApplicationSubmittedEvent.class);
				Set<ConstraintViolation<PANApplicationSubmittedEvent>> violations = validator.validate(panAppObjMapper);
				if (violations.size() > 0) {
					List<String> messages = violations.stream().map(constraintViolation -> ("message = "
							+ constraintViolation.getPropertyPath() + " " + constraintViolation.getMessage()))
							.collect(Collectors.toList());
					throw new PanValidationException("Validation failed for Pan application - " + messages);
				}
			}
		} catch (IOException e) {
			throw new PanValidationException("Validation failed for Pan application - message = " + e.getMessage());
		}
	}

}
