package sg.gov.mpa.msw.prearrivalapplicationservice.model.event;

import java.time.LocalDateTime;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@Valid
@JsonIgnoreProperties(ignoreUnknown = true)
public class DGDApplicationDocumentEvent {

	@NotNull(message = "Document Id is required and cannot be null and size should not exceed 20 characters.")
	private Integer documentId;

	@Size(min = 1, max = 100, message = "Document Type is required and size should not exceed 100 characters.")
	private String documentType;

	@Size(max = 250, message = "Document Type Other size should not exceed 250 characters.")
	private String documentTypeOther;

	@NotNull(message = "Created On should not be null.")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-11-29 21:20:00")
	private LocalDateTime createdOn;

	@Size(min = 1, max = 100, message = "Created By is required and size should not exceed 100 characters.")
	private String createdBy;

}
