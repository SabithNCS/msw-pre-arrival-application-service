package sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration;

public enum PreArrivalApplicationStatus {

	IN_PROGRESS, PROCESSING, APPROVED, REJECTED;

}
