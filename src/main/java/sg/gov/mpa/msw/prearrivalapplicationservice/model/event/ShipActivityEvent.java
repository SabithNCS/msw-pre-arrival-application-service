package sg.gov.mpa.msw.prearrivalapplicationservice.model.event;

import java.time.LocalDateTime;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ShipArrivalDirection;

@Data
@Valid
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShipActivityEvent {

	@NotNull
	private Integer sequenceNumber;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-12-31 14:05:00")
	private LocalDateTime fromDate;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-12-31 14:05:00")
	private LocalDateTime toDate;

	@Size(max = 30, message = "Location cannot be more than 30 characters.")
	private String location;

	@Size(max = 15, message = "Latitude cannot be more than 15 characters.")
	private String latitude;

	@Size(max = 15, message = "Longitude cannot be more than 15 characters.")
	private String longitude;

	private ShipArrivalDirection shipArrivalDirection;

	@Size(max = 50, message = "Activity cannot be more than 50 characters.")
	private String activity;

	@Size(max = 50, message = "Security Measure cannot be more than 50 characters.")
	private String securityMeasure;

	private Long panApplicationId;

	@NotNull
	@Size(max = 100, message = "Created By should not exceed 100 characters.")
	private String createdBy;

}
