package sg.gov.mpa.msw.prearrivalapplicationservice.transformer;

import org.springframework.stereotype.Component;

import sg.gov.mpa.msw.core.interfaces.SharedObjectToEntityTransformer;
import sg.gov.mpa.msw.core.transformer.AbstractCopyPropertiesTransformer;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.PreArrivalApplication;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.sharedobject.PreArrivalApplicationSharedObject;

@Component
public class PreArrivalApplicationSharedObjectTransformer
		extends AbstractCopyPropertiesTransformer<PreArrivalApplicationSharedObject, PreArrivalApplication>
		implements SharedObjectToEntityTransformer<PreArrivalApplicationSharedObject, PreArrivalApplication, String> {

}
