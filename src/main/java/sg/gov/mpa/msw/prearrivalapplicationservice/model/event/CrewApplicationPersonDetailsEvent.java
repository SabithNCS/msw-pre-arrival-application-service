package sg.gov.mpa.msw.prearrivalapplicationservice.model.event;

import java.time.LocalDateTime;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.CrewPassengerPersonType;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.CrewPassengerSurvivorType;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.Gender;

@Data
@Valid
@JsonIgnoreProperties(ignoreUnknown = true)
public class CrewApplicationPersonDetailsEvent {

	@NotNull(message = "Sequence Number cannot be null")
	private Integer sequenceNumber;

	@NotNull(message = "Person Type is required")
	private CrewPassengerPersonType crewPassengerPersonType;

	private CrewPassengerSurvivorType crewPassengerSurvivorType;

	@Size(min = 1, max = 40, message = "Person Name is required.")
	private String personName;

	@NotNull(message = "Date Of Birth cannot be null")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-11-29 21:20:00")
	private LocalDateTime dateOfBirth;

	@Size(min = 1, max = 50, message = "Nationality is required.")
	private String nationality;

	@NotNull(message = "Gender cannot be null")
	private Gender gender;

	@NotNull(message = "Created On is required")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-11-29 21:20:00")
	private LocalDateTime createdOn;

	@Size(min = 1, max = 100, message = "Created By should not be greater then 100")
	private String createdBy;

	@Size(max = 15)
	private String malaysianId;

	@Size(max = 15)
	private String singaporeId;

	@Size(max = 50)
	private String passportNumber;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-11-29 21:20:00")
	private LocalDateTime passportExpiryDate;

}
