package sg.gov.mpa.msw.prearrivalapplicationservice.model.event;

import java.time.LocalDateTime;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import sg.gov.mpa.msw.core.event.MSWKafkaEvent;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationSubmissionStatus;

@Data
@Valid
@JsonIgnoreProperties(ignoreUnknown = true)
public class PANApplicationActionedByAgencyEvent implements MSWKafkaEvent {

	@Size(min = 1, max = 250, message = "MSW Application Reference is required and size should not exceed 250 characters.")
	private String mswReferenceId;

	@Size(max = 50, message = "External Application Reference cannot be more than 50 characters.")
	private String externalReferenceId;

	private ApplicationSubmissionStatus status;

	@Size(max = 30, message = "Remarks cannot be more than 30 characters.")
	private String remarks;

	@Size(max = 100, message = "Processed By cannot be more than 100 characters.")
	private String processedBy;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-09-21 21:20:00")
	private LocalDateTime processedDateTime;

	@Override
	public String getRoutingKey() {
		return "";
	}

}
