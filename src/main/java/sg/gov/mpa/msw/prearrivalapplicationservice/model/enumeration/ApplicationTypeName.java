package sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration;

public enum ApplicationTypeName {
	PAN, 
	NOA
}
