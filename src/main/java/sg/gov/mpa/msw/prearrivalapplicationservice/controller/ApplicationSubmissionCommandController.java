package sg.gov.mpa.msw.prearrivalapplicationservice.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import sg.gov.mpa.msw.core.command.controller.AbstractSharedObjectCommandController;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationSubmission;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.sharedobject.ApplicationSubmissionSharedObject;

@RestController
@Api
@RequestMapping("/application-submissions")
public class ApplicationSubmissionCommandController
		extends AbstractSharedObjectCommandController<ApplicationSubmission, Long, ApplicationSubmissionSharedObject> {

}
