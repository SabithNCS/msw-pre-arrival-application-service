package sg.gov.mpa.msw.prearrivalapplicationservice.exception;

public class PanValidationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public PanValidationException(String message) {
		super(message);
	}
}
