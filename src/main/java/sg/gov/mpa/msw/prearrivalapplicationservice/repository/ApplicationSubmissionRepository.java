package sg.gov.mpa.msw.prearrivalapplicationservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationSubmission;

public interface ApplicationSubmissionRepository
		extends JpaRepository<ApplicationSubmission, Long>, JpaSpecificationExecutor<ApplicationSubmission> {

}
