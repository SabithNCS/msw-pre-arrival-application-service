package sg.gov.mpa.msw.prearrivalapplicationservice.model.event;

import java.time.LocalDateTime;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@Valid
@JsonIgnoreProperties(ignoreUnknown = true)
public class VesselHealthDeceasedDetailEvent {

	@Size(min = 1, max = 100, message = "Declaration remarks cannot be more than 100 characters.")
	public String deceasedName;

	@Size(min = 1, max = 20, message = "Uin Type cannot be more than 20 characters.")
	public String uinType;

	@Size(min = 1, max = 50, message = "Uin cannot be more than 50 characters.")
	public String uin;

	@NotNull(message = "Date Of Birth is required")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-09-21 21:20:00")
	public LocalDateTime dateOfBirth;

	@NotNull(message = "Date Of Death is required")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-09-21 21:20:00")
	public LocalDateTime dateOfDeath;

	@Size(min = 1, max = 100, message = "Citizenship cannot be more than 100 characters.")
	public String citizenship;

	@Size(min = 1, max = 20, message = "gender cannot be more than 20 characters.")
	public String gender;

	@Size(min = 1, max = 10, message = "Marital Status cannot be more than 10 characters.")
	public String maritalStatus;

	@NotNull(message = "Created On is required")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-09-21 21:20:00")
	private LocalDateTime createdOn;

}
