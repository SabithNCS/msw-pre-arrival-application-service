package sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration;

public enum CrewPassengerSurvivorType {

	SURVIVOR, REFUGEE, STOWAWAY, DEAD_BODY, UNAUTHORISED_PERSON;

}
