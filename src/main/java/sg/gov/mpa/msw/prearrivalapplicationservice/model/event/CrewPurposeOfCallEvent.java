package sg.gov.mpa.msw.prearrivalapplicationservice.model.event;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@Valid
@JsonIgnoreProperties(ignoreUnknown = true)
public class CrewPurposeOfCallEvent {

	@Size(min = 1, max = 100, message = "Purpose Of Call should not be greater then 100")
	private String purposeOfCall;

}
