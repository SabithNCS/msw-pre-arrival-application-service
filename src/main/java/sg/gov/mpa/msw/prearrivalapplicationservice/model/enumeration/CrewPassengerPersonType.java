package sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration;

public enum CrewPassengerPersonType {

	SURVIVOR, CREW, THROUGH_PASSENGER, DISEMBARKATION_PASSENGER;

}
