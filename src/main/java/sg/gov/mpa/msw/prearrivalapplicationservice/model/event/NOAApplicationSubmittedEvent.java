package sg.gov.mpa.msw.prearrivalapplicationservice.model.event;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import sg.gov.mpa.msw.core.event.MSWKafkaEvent;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.SGSecurityLevel;

@Data
@Valid
@JsonIgnoreProperties(ignoreUnknown = true)
public class NOAApplicationSubmittedEvent implements MSWKafkaEvent {

	@NotNull
	private Long voyageId;

	@NotNull
	@Size(max = 250, message = "MSW Application Referance cannot be more than 50 characters.")
	private String mswApplicationReferance;

	@Size(max = 50, message = "External Application Reference cannot be more than 50 characters.")
	private String externalReferenceId;

	@NotNull
	@Size(max = 100, message = "Application Id cannot be more than 100 characters.")
	private String applicantId;

	private Double arrivalDraftsForwards;

	private Double arrivalDraftsMid;

	private Double arrivalDraftsAfter;

	private Double airDraft;

	@NotNull
	@Size(max = 30, message = "Inmarsat Number cannot be more than 30 characters.")
	private String inmarsatNumber;

	@NotNull
	@Size(max = 100, message = "Company Security Officer cannot be more than 100 characters.")
	private String companySecurityOfficer;

	@NotNull
	@Size(max = 15, message = "Cso Phone Number cannot be more than 15 characters.")
	private String csoPhoneNumber;

	@Size(max = 100, message = "Agent Name cannot be more than 100 characters.")
	private String agentName;

	@Size(max = 15, message = "Agent Phone Number cannot be more than 15 characters.")
	private String agentPhoneNumber;

	@Size(max = 15, message = "Agent Fax Number cannot be more than 15 characters.")
	private String agentFaxNumber;

	@Email
	@Size(max = 100, message = "Agent Email Address be more than 100 characters.")
	private String agentEmailAddress;

	@Email
	@Size(max = 100, message = "Ship Email Address cannot be more than 100 characters.")
	private String shipEmailAddress;

	@Size(max = 100, message = "Voyage Number cannot be more than 100 characters.")
	private String voyageNumber;

	private Double anchorageDuration;

	@NotNull
	@Size(max = 250, message = "Cargo Description cannot be more than 250 characters.")
	private String cargoDescription;

	private Boolean securityPlanExists;

	private SGSecurityLevel sgSecurityLevel;

	private Boolean securityPersonnelOnboard;

	private Long slopQuantity;

	private Long sludgeQuantity;

	private Boolean slopSludgeOnBoard;

	private String slopSludgeContent;

	private String slopSludgeReceptionFacility;

	@ApiModelProperty(example = "false")
	private Boolean isNOA = false;

	private Long height;

	@Size(max = 250, message = "Name Of Person Repporting cannot be more than 100 characters.")
	private String nameOfPersonReporting;

	private Boolean isSafetyAndSecurityAffected;

	@NotNull
	@Size(max = 100, message = "Created By should not exceed 100 characters.")
	private String createdBy;

	private List<PurposeOfCallEvent> purposeOfCallEvents = new ArrayList<>();

	@Override
	public String getRoutingKey() {
		// TODO Auto-generated method stub
		return "";
	}
}
