package sg.gov.mpa.msw.prearrivalapplicationservice.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;

import lombok.Data;

@Data
@Configuration
@EnableKafka
public class KafkaConfig {

	@Value("${spring.kafka.bootstrap-servers}")
	private String bootStrapServer;

	@Value("${spring.kafka.client-id}")
	private String clientId;

	@Value("${kafka.topic.prefix}")
	private String topicPrefix;

}
