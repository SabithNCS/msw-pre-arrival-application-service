package sg.gov.mpa.msw.prearrivalapplicationservice.model.event;

import java.time.LocalDateTime;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@Valid
@JsonIgnoreProperties(ignoreUnknown = true)
public class VesselHealthRefugeeInformationEvent {

	@NotNull(message = "Number Of Sick Refugees is required")
	public Integer numberOfSickRefugees;

	@NotNull(message = "Total Number Of Refugees is required")
	public Integer totalNumberOfRefugees;

	@NotNull(message = "Created On is required")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-09-21 21:20:00")
	private LocalDateTime createdOn;

}
