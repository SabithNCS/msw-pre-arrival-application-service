package sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration;

public enum PanShipCertificateStatus {
	SUBMITTED, ACCEPTED, RETURNED_FOR_AMENDMENT, REJECTED
}
