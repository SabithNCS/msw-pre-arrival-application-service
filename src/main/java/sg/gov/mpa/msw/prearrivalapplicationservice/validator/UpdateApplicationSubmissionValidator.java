package sg.gov.mpa.msw.prearrivalapplicationservice.validator;

import java.util.Optional;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import sg.gov.mpa.msw.core.command.ChangeEntityCommand;
import sg.gov.mpa.msw.core.command.validator.CommandValidator;
import sg.gov.mpa.msw.prearrivalapplicationservice.exception.ApplicationUpdateException;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationSubmission;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationSubmissionStatus;
import sg.gov.mpa.msw.prearrivalapplicationservice.repository.ApplicationSubmissionRepository;

@SuppressWarnings("rawtypes")
@Component
@AllArgsConstructor
public class UpdateApplicationSubmissionValidator implements CommandValidator<ChangeEntityCommand> {

	ApplicationSubmissionRepository applicationSubmissionRepository;

	@Override
	public boolean isApplicable(ChangeEntityCommand command) {
		return (command.getChangeType() == ChangeEntityCommand.ChangeType.UPDATE
				&& command.getEntity() instanceof ApplicationSubmission);
	}

	@Override
	public void validate(ChangeEntityCommand command) {
		ApplicationSubmission applicationSubmissionEntity = (ApplicationSubmission) command.getEntity();

		Optional<ApplicationSubmission> applicationSubmission = applicationSubmissionRepository
				.findById(applicationSubmissionEntity.getId());
		if (applicationSubmission.isPresent() && (!((applicationSubmission.get()
				.getApplicationSubmissionStatus() == ApplicationSubmissionStatus.DRAFT)
				|| (applicationSubmission.get()
						.getApplicationSubmissionStatus() == ApplicationSubmissionStatus.NOT_SUBMITTED)))) {
			throw new ApplicationUpdateException("Active application in process cannot be updated");
		}
	}

}
