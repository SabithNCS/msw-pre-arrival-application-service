package sg.gov.mpa.msw.prearrivalapplicationservice.exception;

public class NoaValidationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public NoaValidationException(String message) {
		super(message);
	}
}
