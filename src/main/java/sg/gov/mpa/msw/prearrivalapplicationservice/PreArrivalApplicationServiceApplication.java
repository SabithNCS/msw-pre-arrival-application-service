package sg.gov.mpa.msw.prearrivalapplicationservice;

import static sg.gov.mpa.msw.prearrivalapplicationservice.config.DefaultProfileUtil.SPRING_PROFILE_DEVELOPMENT;
import static sg.gov.mpa.msw.prearrivalapplicationservice.config.DefaultProfileUtil.SPRING_PROFILE_PRODUCTION;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collection;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.core.env.Environment;

import lombok.extern.slf4j.Slf4j;
import sg.gov.mpa.msw.core.config.EnableCoreFunctionalities;
import sg.gov.mpa.msw.prearrivalapplicationservice.config.DefaultProfileUtil;

@Slf4j
@EnableCoreFunctionalities
@SpringBootApplication(exclude = KafkaAutoConfiguration.class)
public class PreArrivalApplicationServiceApplication {

	private final Environment env;

	public PreArrivalApplicationServiceApplication(Environment env) {
		this.env = env;
	}

	@PostConstruct
	public void initApplication() {
		Collection<String> activeProfiles = Arrays.asList(env.getActiveProfiles());
		if (activeProfiles.contains(SPRING_PROFILE_DEVELOPMENT) && activeProfiles.contains(SPRING_PROFILE_PRODUCTION)) {
			log.error("You have mis-configured your application! It should not run "
					+ "with both the 'dev' and 'prod' profiles at the same time.");
		}
	}

	public static void main(final String[] args) throws UnknownHostException {
		SpringApplication app = new SpringApplication(
				sg.gov.mpa.msw.prearrivalapplicationservice.PreArrivalApplicationServiceApplication.class);
		DefaultProfileUtil.addDefaultProfile(app);
		Environment env = app.run(args).getEnvironment();
		String protocol = "http";
		if (env.getProperty("server.ssl.key-store") != null) {
			protocol = "https";
		}
		log.info("\n----------------------------------------------------------\n\t"
				+ "Application '{}' is running! Access URLs:\n\t" + "Local: \t\t{}://localhost:{}/swagger-ui.html\n\t"
				+ "External: \t{}://{}:{}/swagger-ui.html\n\t"
				+ "Profile(s): \t{}\n----------------------------------------------------------",
				env.getProperty("spring.application.name"), protocol, env.getProperty("server.port"), protocol,
				InetAddress.getLocalHost().getHostAddress(), env.getProperty("server.port"), env.getActiveProfiles());

		String configServerStatus = env.getProperty("configserver.status");
		log.info(
				"\n----------------------------------------------------------\n\t"
						+ "Config Server: \t{}\n----------------------------------------------------------",
				configServerStatus == null ? "Not found or not setup for this application" : configServerStatus);
	}
}
