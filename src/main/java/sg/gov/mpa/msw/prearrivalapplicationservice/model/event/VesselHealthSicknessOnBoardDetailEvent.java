package sg.gov.mpa.msw.prearrivalapplicationservice.model.event;

import java.time.LocalDateTime;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@Valid
@JsonIgnoreProperties(ignoreUnknown = true)
public class VesselHealthSicknessOnBoardDetailEvent {

	@Size(min = 1, max = 100, message = "Name cannot be more than 100 characters.")
	public String name;

	@Size(min = 1, max = 20, message = "Id Type cannot be more than 20 characters.")
	public String idType;

	@Size(min = 1, max = 50, message = "Id Number cannot be more than 50 characters.")
	public String idNumber;

	@NotNull(message = "Date On Set is required")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-09-21 21:20:00")
	private LocalDateTime dateOnSet;

	@Size(max = 250, message = "Cause Of Sickness cannot be more than 250 characters.")
	public String causeOfSickness;

	@Size(max = 100, message = "Name Of Medical Doctor cannot be more than 100 characters.")
	public String nameOfMedicalDoctor;

	@Size(max = 100, message = "Name Of Medical Company cannot be more than 100 characters.")
	public String nameOfMedicalCompany;

	@Size(max = 100, message = "Citizenship cannot be more than 100 characters.")
	public String citizenship;

	public int age;

	@Size(max = 20, message = "gender cannot be more than 20 characters.")
	public String gender;

	@Size(min = 1, max = 10, message = "Marital Status cannot be more than 10 characters.")
	public String maritalStatus;

	@NotNull(message = "Created On is required")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-09-21 21:20:00")
	private LocalDateTime createdOn;

}
