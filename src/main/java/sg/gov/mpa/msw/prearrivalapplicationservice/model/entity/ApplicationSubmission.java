package sg.gov.mpa.msw.prearrivalapplicationservice.model.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import sg.gov.mpa.msw.core.interfaces.BaseEntity;
import sg.gov.mpa.msw.core.interfaces.LogicalDelete;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationSubmissionStatus;

@Data
@Entity
@EqualsAndHashCode(of = { "id" })
@Table(name = "APPLICATION_SUBMISSION")
public class ApplicationSubmission implements BaseEntity<Long>, LogicalDelete {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Column(name = "APPLICATION_DATA")
	private String applicationData;

	@Enumerated(value = EnumType.STRING)
	@Column(name = "APPLICATION_STATUS")
	private ApplicationSubmissionStatus applicationSubmissionStatus;

	@Column(name = "EXTERNAL_APPLICATION_ID")
	private String externalApplicationId;

	@Column(name = "PROCESSED_BY")
	private String processedBy;

	@Column(name = "PROCESSED_ON")
	private LocalDateTime processedOn;

	@Column(name = "CUTOFF_DATE")
	private LocalDateTime cutOffDate;

	@Version
	@Column(name = "LOCK_VERSION")
	private Long lockVersion;

	@Column(name = "CREATED_ON")
	private LocalDateTime createdOn;

	@Column(name = "CREATED_BY")
	private String createdBy;

	@Column(name = "LAST_UPDATED_ON")
	private LocalDateTime lastUpdatedOn;

	@Column(name = "LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Column(name = "IS_DELETED")
	private Boolean deleted = false;

	@ManyToOne
	@JoinColumn(name = "APPLICATION_REFERENCE")
	private PreArrivalApplication preArrivalApplication;

	@ManyToOne
	@JoinColumn(name = "APPLICATION_TYPE")
	private ApplicationType applicationType;

	@Override
	public void setIsDeleted(boolean isDeleted) {
		this.deleted = isDeleted;
	}
}
