package sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration;

public enum GoodsPackageType {

	BOX, BAG, DRUM, CONTAINER, CYLINDER, TANK, OTHERS

}
