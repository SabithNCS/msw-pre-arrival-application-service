package sg.gov.mpa.msw.prearrivalapplicationservice.service;

import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationSubmission;

public interface ApplicationSubmittedEventProducerService {
	void sendApplicationSubmittedEvent(ApplicationSubmission entity);
	boolean isApplicable(ApplicationSubmission entity);
}
