package sg.gov.mpa.msw.prearrivalapplicationservice.model.event;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import sg.gov.mpa.msw.core.event.MSWKafkaEvent;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ContactMethod;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.CrewApplicationType;

@Data
@Valid
@JsonIgnoreProperties(ignoreUnknown = true)
public class CrewApplicationSubmittedEvent implements MSWKafkaEvent {

	@NotNull(message = "Voyage Id cannot be blank")
	private Long voyageId;

	@Size(min = 1, max = 250, message = "Msw Reference Id cannot be blank")
	private String mswApplicationReference;

	@Size(max = 100)
	private String externalApplicationReference;

	@Size(min = 1, max = 100, message = "Applicant Id cannot be blank")
	private String applicantId;

	@NotNull(message = "Application Type cannot be blank")
	private CrewApplicationType crewApplicationType;

	@Size(max = 40)
	private String contactPerson;

	private ContactMethod contactMethod;

	private Long telephoneNumber;

	private Long mobileNumber;

	@NotNull(message = "Expected Time Of Arrival cannot be blank")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-11-29 21:20:00")
	private LocalDateTime expectedTimeOfArrival;

	@NotNull(message = "Expected Time Of Departure cannot be blank")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-11-29 21:20:00")
	private LocalDateTime expectedTimeOfDeparture;

	private Boolean licensePermitExists = Boolean.FALSE;

	@Size(max = 5)
	private String lastEmbarkationPlace;

	@Size(max = 5)
	private String nextDestination;

	@Size(max = 5)
	private String immigrationAnchorage;

	private Boolean specificIslandIndicator = Boolean.FALSE;

	@Size(max = 256)
	private String specificIslandDetails;

	@Size(max = 40)
	private String ownerCharter;

	private Boolean applyForAic = Boolean.FALSE;

	private Boolean vesselOfTow = Boolean.FALSE;

	@Size(max = 256)
	private String towDetails;

	@Size(max = 20)
	private String typeOfCargo;

	@Size(max = 256)
	private String otherCargo;

	@NotNull(message = "Created On is required")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-11-29 21:20:00")
	private LocalDateTime createdOn;

	@Size(min = 1, max = 100, message = "Created By should not be greater then 100")
	private String createdBy;

	private Boolean anySurvivorOnboard = Boolean.FALSE;

	@Valid
	private List<CrewPurposeOfCallEvent> crewPurposeOfCallEvents = new ArrayList<>();

	@Valid
	private List<CrewApplicationPersonDetailsEvent> crewApplicationPersonDetailsEvents = new ArrayList<>();

	@Override
	public String getRoutingKey() {
		return "";
	}
}
