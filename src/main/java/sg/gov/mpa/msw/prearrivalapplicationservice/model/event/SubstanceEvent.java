package sg.gov.mpa.msw.prearrivalapplicationservice.model.event;

import java.time.LocalDateTime;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.IbcHazardCode;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.OperationStatus;

@Data
@Valid
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubstanceEvent {

	@NotNull
	private Integer sequenceNumber;

	@Size(min = 1, max = 50, message = "SubstanceEvent code should not exceed 50 characters.")
	private String substanceCode;

	@Size(max = 256, message = "SubstanceEvent Name should not exceed 256 characters.")
	private String substanceName;

	@Size(max = 4, message = "UnNumber should not exceed 4 characters.")
	private String unNumber;

	@Size(max = 50, message = "Pollution Catrgory should not exceed 50 characters.")
	private String pollutionCategory;

	@Size(max = 10, message = "Cargo Group should not exceed 10 characters.")
	private String cargoGroup;

	private Double flashPoint;

	@Size(max = 15, message = "Stewage On Board should not exceed 15 characters.")
	private String stewageOnBoard;

	@Size(max = 5, message = "Location of operation should not exceed 5 characters.")
	private String locationOfOperation;

	private Double quantity;

	@NotNull
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-09-21 21:20:00")
	private LocalDateTime createdOn;

	@Size(min = 1, max = 100, message = "Created By is required. Created By should not exceed 100 characters.")
	private String createdBy;

	private OperationStatus operation;

	private IbcHazardCode ibcHazardCode;
}
