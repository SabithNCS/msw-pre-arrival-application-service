package sg.gov.mpa.msw.prearrivalapplicationservice.model.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import sg.gov.mpa.msw.core.interfaces.BaseEntity;
import sg.gov.mpa.msw.core.interfaces.LogicalDelete;

@Data
@Entity
@EqualsAndHashCode(of = { "id" })
@Table(name = "APPLICATION_TYPE")
public class ApplicationType implements BaseEntity<String>, LogicalDelete {

	@Id
	@Column(name = "CODE")
	private String id;

	@Column(name = "DESCRIPTION")
	private String description;

	@Version
	@Column(name = "LOCK_VERSION")
	private Long lockVersion;

	@Column(name = "CREATED_ON")
	private LocalDateTime createdOn;

	@Column(name = "CREATED_BY")
	private String createdBy;

	@Column(name = "LAST_UPDATED_ON")
	private LocalDateTime lastUpdatedOn;

	@Column(name = "LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Column(name = "IS_DELETED")
	private Boolean deleted;

	@Override
	public void setIsDeleted(boolean isDeleted) {
		this.deleted = isDeleted;
	}

}
