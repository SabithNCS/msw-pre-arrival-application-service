package sg.gov.mpa.msw.prearrivalapplicationservice.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.serializer.JsonSerializer;

import lombok.AllArgsConstructor;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.event.NOAApplicationSubmittedEvent;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.event.PANApplicationSubmittedEvent;

@Configuration
@AllArgsConstructor
public class KafkaProducerConfig {

	private final KafkaConfig kafkaConfig;

	private final MappingJackson2HttpMessageConverter springJacksonConverter;

	@Bean
	public KafkaTemplate<String, PANApplicationSubmittedEvent> panApplicationSubmittedEventKafkaTemplate() {
		return new KafkaTemplate<>(panApplicationSubmittedEventProducerFactory());
	}

	@Bean
	public DefaultKafkaProducerFactory<String, PANApplicationSubmittedEvent> panApplicationSubmittedEventProducerFactory() {
		final DefaultKafkaProducerFactory<String, PANApplicationSubmittedEvent> producerFactory = new DefaultKafkaProducerFactory<>(
				producerConfigs(PANApplicationSubmittedEvent.class.getSimpleName()));
		producerFactory.setValueSerializer(getJsonSerializer());

		return producerFactory;
	}

	@Bean
	public KafkaTemplate<String, NOAApplicationSubmittedEvent> noaApplicationSubmittedEventKafkaTemplate() {
		return new KafkaTemplate<>(noaApplicationSubmittedEventProducerFactory());
	}

	@Bean
	public DefaultKafkaProducerFactory<String, NOAApplicationSubmittedEvent> noaApplicationSubmittedEventProducerFactory() {

		final DefaultKafkaProducerFactory<String, NOAApplicationSubmittedEvent> producerFactory = new DefaultKafkaProducerFactory<>(
				producerConfigs(NOAApplicationSubmittedEvent.class.getSimpleName()));
		producerFactory.setValueSerializer(getJsonSerializer());

		return producerFactory;
	}

	private Map<String, Object> producerConfigs(String clientIdSuffix) {
		Map<String, Object> props = new HashMap<>();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaConfig.getBootStrapServer());
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
		props.put(ProducerConfig.CLIENT_ID_CONFIG, kafkaConfig.getClientId().concat("-").concat(clientIdSuffix));

		return props;
	}

	private <T> JsonSerializer<T> getJsonSerializer() {
		return new JsonSerializer<>(springJacksonConverter.getObjectMapper());
	}
}
