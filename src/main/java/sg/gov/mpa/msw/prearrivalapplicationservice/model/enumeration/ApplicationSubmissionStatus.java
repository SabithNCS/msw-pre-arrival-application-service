package sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration;

public enum ApplicationSubmissionStatus {
	DRAFT, NOT_SUBMITTED, SUBMITTED, PROCESSING, APPROVED, REJECTED, RETURNED_FOR_MORE_INFO, WITHDRAWN, DELETED
}
