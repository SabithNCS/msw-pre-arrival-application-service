package sg.gov.mpa.msw.prearrivalapplicationservice.transformer;

import org.springframework.stereotype.Component;

import sg.gov.mpa.msw.core.interfaces.EntityToSharedObjectTransformer;
import sg.gov.mpa.msw.core.transformer.AbstractCopyPropertiesTransformer;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.PreArrivalApplication;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.sharedobject.PreArrivalApplicationSharedObject;

@Component
public class PreArrivalApplicationEntityTransformer
		extends AbstractCopyPropertiesTransformer<PreArrivalApplication, PreArrivalApplicationSharedObject>
		implements EntityToSharedObjectTransformer<PreArrivalApplication, PreArrivalApplicationSharedObject, String> {

}
