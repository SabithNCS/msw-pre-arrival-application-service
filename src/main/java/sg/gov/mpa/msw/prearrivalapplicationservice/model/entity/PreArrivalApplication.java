package sg.gov.mpa.msw.prearrivalapplicationservice.model.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import sg.gov.mpa.msw.core.interfaces.BaseEntity;
import sg.gov.mpa.msw.core.interfaces.LogicalDelete;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.PreArrivalApplicationStatus;

@Data
@Entity
@EqualsAndHashCode(of = { "id" })
@Table(name = "PRE_ARRIVAL_APPLICATION")
public class PreArrivalApplication implements BaseEntity<String>, LogicalDelete {

	@Id
	@Column(name = "ID")
	private String id;

	@Column(name = "VESSEL_ID")
	private Long vesselId;

	@Column(name = "VOYAGE_ID")
	private Long voyageId;

	@Enumerated(value = EnumType.STRING)
	@Column(name = "PRE_ARRIVAL_APPLICATION_STATUS")
	private PreArrivalApplicationStatus preArrivalApplicationStatus;

	@Column(name = "CREATED_ON")
	private LocalDateTime createdOn;

	@Column(name = "CREATED_BY")
	private String createdBy;

	@Column(name = "LAST_UPDATED_ON")
	private LocalDateTime lastUpdatedOn;

	@Column(name = "LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Column(name = "IS_DELETED")
	private Boolean deleted = false;

	@Version
	@Column(name = "LOCK_VERSION")
	private Long lockVersion;

	@Override
	public void setIsDeleted(boolean isDeleted) {
		this.deleted = isDeleted;
	}

}
