package sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration;

public enum Gender {

	MALE, FEMALE, NOT_KNOWN, NOT_APPLICABLE;

}
