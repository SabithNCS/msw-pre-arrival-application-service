package sg.gov.mpa.msw.prearrivalapplicationservice.model.event;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PurposeOfCallEvent {

	private String purposeOfCall;

	private Long panId;

}
