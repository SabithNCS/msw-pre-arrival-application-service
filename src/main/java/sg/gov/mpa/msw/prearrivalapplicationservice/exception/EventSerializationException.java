package sg.gov.mpa.msw.prearrivalapplicationservice.exception;

public class EventSerializationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public EventSerializationException(String message) {
		super(message);
	}

}
