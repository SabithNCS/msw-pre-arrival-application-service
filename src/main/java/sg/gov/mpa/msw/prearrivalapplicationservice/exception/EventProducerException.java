package sg.gov.mpa.msw.prearrivalapplicationservice.exception;

public class EventProducerException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public EventProducerException(String message) {
		super(message);
	}
}
