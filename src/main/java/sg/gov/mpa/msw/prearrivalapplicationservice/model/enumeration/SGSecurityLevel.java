package sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration;

public enum SGSecurityLevel {
	NORMAL, MEDIUM, HIGH
}
