package sg.gov.mpa.msw.prearrivalapplicationservice.exception;

public class ApplicationDeleteException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ApplicationDeleteException(String message) {
		super(message);
	}
}
