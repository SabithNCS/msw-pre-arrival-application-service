package sg.gov.mpa.msw.prearrivalapplicationservice.service;

import java.util.List;

import org.springframework.stereotype.Service;

import sg.gov.mpa.msw.core.command.service.AbstractBaseEntityCommandService;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationSubmission;
import sg.gov.mpa.msw.prearrivalapplicationservice.repository.ApplicationSubmissionRepository;

@Service
public class PreArrivalApplicationService extends AbstractBaseEntityCommandService<ApplicationSubmission, Long> {
	private List<ApplicationSubmittedEventProducerService> preArrivalApplicationEventProducerService;

	public PreArrivalApplicationService(final ApplicationSubmissionRepository repository,
			 List<ApplicationSubmittedEventProducerService> preArrivalApplicationEventProducerService) {
		super(repository);
		this.preArrivalApplicationEventProducerService = preArrivalApplicationEventProducerService;
	}

	@Override
	protected void afterUpdate(ApplicationSubmission entity) {
		preArrivalApplicationEventProducerService.stream().filter(applicationType -> applicationType.isApplicable(entity)).forEach(eventProducer -> eventProducer.sendApplicationSubmittedEvent(entity));
	}

}
