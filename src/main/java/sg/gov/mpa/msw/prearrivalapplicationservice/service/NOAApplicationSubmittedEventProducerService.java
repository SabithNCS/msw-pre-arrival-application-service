package sg.gov.mpa.msw.prearrivalapplicationservice.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import sg.gov.mpa.msw.prearrivalapplicationservice.config.KafkaConfig;
import sg.gov.mpa.msw.prearrivalapplicationservice.exception.EventProducerException;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationSubmission;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationSubmissionStatus;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationTypeName;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.event.NOAApplicationSubmittedEvent;

@Service
public class NOAApplicationSubmittedEventProducerService implements ApplicationSubmittedEventProducerService {

	@Value("${kafka.topic.name.noas-submission}")
	private String noaSubmissionTopic;

	private final KafkaConfig kafkaConfig;

	private final KafkaTemplate<String, NOAApplicationSubmittedEvent> noaApplicationSubmittedEventKafkaTemplate;

	private final MappingJackson2HttpMessageConverter springJacksonConverter;

	public NOAApplicationSubmittedEventProducerService(KafkaConfig kafkaConfig,
			KafkaTemplate<String, NOAApplicationSubmittedEvent> noaApplicationSubmittedEventKafkaTemplate,
			MappingJackson2HttpMessageConverter springJacksonConverter) {
		this.kafkaConfig = kafkaConfig;
		this.noaApplicationSubmittedEventKafkaTemplate = noaApplicationSubmittedEventKafkaTemplate;
		this.springJacksonConverter = springJacksonConverter;
	}

	public void sendApplicationSubmittedEvent(final ApplicationSubmission entity) {
		if (entity.getApplicationSubmissionStatus() == ApplicationSubmissionStatus.SUBMITTED) {
			ObjectMapper objectMapper = springJacksonConverter.getObjectMapper();
			NOAApplicationSubmittedEvent noaApplicationSubmittedEvent = null;
			try {
				noaApplicationSubmittedEvent = objectMapper.readValue(entity.getApplicationData(),
						NOAApplicationSubmittedEvent.class);
			} catch (IOException e) {
				throw new EventProducerException("Exception while producing NOA event");
			}
			noaApplicationSubmittedEventKafkaTemplate.send(kafkaConfig.getTopicPrefix() + noaSubmissionTopic,
					noaApplicationSubmittedEvent);
		}
	}

	public boolean isApplicable(final ApplicationSubmission entity) {
		return ApplicationTypeName.NOA.name().equals(entity.getApplicationType().getId());
	}

}
