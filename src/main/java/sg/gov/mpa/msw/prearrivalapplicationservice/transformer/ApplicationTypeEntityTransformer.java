package sg.gov.mpa.msw.prearrivalapplicationservice.transformer;

import org.springframework.stereotype.Component;

import sg.gov.mpa.msw.core.interfaces.EntityToSharedObjectTransformer;
import sg.gov.mpa.msw.core.transformer.AbstractCopyPropertiesTransformer;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationType;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.sharedobject.ApplicationTypeSharedObject;

@Component
public class ApplicationTypeEntityTransformer
		extends AbstractCopyPropertiesTransformer<ApplicationType, ApplicationTypeSharedObject>
		implements EntityToSharedObjectTransformer<ApplicationType, ApplicationTypeSharedObject, String> {

}
