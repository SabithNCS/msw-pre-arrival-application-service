package sg.gov.mpa.msw.prearrivalapplicationservice.validator;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.AllArgsConstructor;
import sg.gov.mpa.msw.core.command.ChangeEntityCommand;
import sg.gov.mpa.msw.core.command.validator.CommandValidator;
import sg.gov.mpa.msw.prearrivalapplicationservice.exception.NoaValidationException;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationSubmission;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationSubmissionStatus;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationTypeName;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.event.NOAApplicationSubmittedEvent;

@SuppressWarnings("rawtypes")
@Component
@AllArgsConstructor
public class NoaApplicationSubmissionValidator implements CommandValidator<ChangeEntityCommand> {

	private final Validator validator;

	private final MappingJackson2HttpMessageConverter springJacksonConverter;

	@Override
	public boolean isApplicable(ChangeEntityCommand command) {
		return ((command.getChangeType() == ChangeEntityCommand.ChangeType.UPDATE)
				&& (command.getEntity() instanceof ApplicationSubmission) && (ApplicationTypeName.NOA.name()
						.equals(((ApplicationSubmission) command.getEntity()).getApplicationType().getId())));
	}

	@Override
	public void validate(ChangeEntityCommand command) {
		ApplicationSubmission applicationSubmissionEntity = (ApplicationSubmission) command.getEntity();
		String noaApplicationData = applicationSubmissionEntity.getApplicationData();
		ApplicationSubmissionStatus applicationStatus = applicationSubmissionEntity.getApplicationSubmissionStatus();
		ObjectMapper objectMapper = springJacksonConverter.getObjectMapper();
		try {
			JsonNode noaJsonObj = objectMapper.readTree(noaApplicationData);
			if ((noaJsonObj == null || (noaJsonObj != null && noaJsonObj.size() < 1))
					&& (applicationStatus == ApplicationSubmissionStatus.SUBMITTED
							|| applicationStatus == ApplicationSubmissionStatus.DRAFT))
				throw new NoaValidationException("NOA Application must have atleast one field");

			if (applicationStatus == ApplicationSubmissionStatus.SUBMITTED) {
				NOAApplicationSubmittedEvent noaAppObjMapper = objectMapper.readValue(noaApplicationData,
						NOAApplicationSubmittedEvent.class);
				Set<ConstraintViolation<NOAApplicationSubmittedEvent>> violations = validator.validate(noaAppObjMapper);
				if (violations.size() > 0) {
					List<String> messages = violations.stream().map(constraintViolation -> ("message = "
							+ constraintViolation.getPropertyPath() + " " + constraintViolation.getMessage()))
							.collect(Collectors.toList());
					throw new NoaValidationException("Validation failed for Noa application - " + messages);
				}
			}
		} catch (IOException e) {
			throw new NoaValidationException("Validation failed for Noa application - message = " + e.getMessage());
		}
	}

}
