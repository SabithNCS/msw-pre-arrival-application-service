package sg.gov.mpa.msw.prearrivalapplicationservice.exception;

public class ApplicationUpdateException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ApplicationUpdateException(String message) {
		super(message);
	}
}
