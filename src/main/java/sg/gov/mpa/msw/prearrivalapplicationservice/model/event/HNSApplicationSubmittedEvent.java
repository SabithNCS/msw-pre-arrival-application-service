package sg.gov.mpa.msw.prearrivalapplicationservice.model.event;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import sg.gov.mpa.msw.core.event.MSWKafkaEvent;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ContactMethod;

@Data
@Valid
@JsonIgnoreProperties(ignoreUnknown = true)
public class HNSApplicationSubmittedEvent implements MSWKafkaEvent {

	@Size(min = 1, max = 100, message = "Application Id cannot be more than 100 characters.")
	private String applicantId;

	@NotNull(message = "Voyage Id is required")
	private Long voyageId;

	@Size(max = 40, message = "Contact Person cannot be more than 40 characters.")
	private String contactPerson;

	private Long mobileNumber;

	@Size(max = 50, message = "Last Port Country should not exceed 50 characters.")
	private String lastPortCountry;

	@Size(max = 50, message = "Last Port code should not exceed 50 characters.")
	private String lastPortCode;

	@Size(max = 13, message = "Container Number should not exceed 13 characters")
	private String containerNumber;

	@Size(min = 1, max = 250, message = "Msw Application Reference cannot be more than 250 characters.")
	private String mswApplicationReference;

	@NotNull(message = "Expected Time Of Arrival is required.")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-09-21 21:20:00")
	private LocalDateTime expectedTimeOfArrival;

	@NotNull(message = "Expected Time Of Deparature is required.")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-09-21 21:20:00")
	private LocalDateTime expectedTimeOfDeparature;

	private ContactMethod contactMethod;

	@NotNull(message = "Created On is required")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-09-21 21:20:00")
	private LocalDateTime createdOn;

	@Size(min = 1, max = 100, message = "Created By is required. Created By should not exceed 100 characters.")
	private String createdBy;

	private List<SubstanceEvent> substanceEvents = new ArrayList<>();

	@Override
	public String getRoutingKey() {
		return "";
	}
}
