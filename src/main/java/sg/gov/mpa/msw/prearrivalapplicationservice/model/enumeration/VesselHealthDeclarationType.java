package sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration;

public enum VesselHealthDeclarationType {

	SICK_ON_BOARD, ARRIVING_FROM_INFECTED_PORT, DEATH_ON_BOARD, REFUGEE_ON_BOARD
}
