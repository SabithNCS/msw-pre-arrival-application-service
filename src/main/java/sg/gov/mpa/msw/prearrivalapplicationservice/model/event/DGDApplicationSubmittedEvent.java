package sg.gov.mpa.msw.prearrivalapplicationservice.model.event;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import sg.gov.mpa.msw.core.event.MSWKafkaEvent;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ContactMethod;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.GoodsPackageType;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.OperationStatus;

@Data
@Valid
@JsonIgnoreProperties(ignoreUnknown = true)
public class DGDApplicationSubmittedEvent implements MSWKafkaEvent {

	@Size(min = 1, max = 250, message = "MSW Application Reference is required and size should not exceed 250 characters.")
	private String mswApplicationReference;

	@Size(min = 1, max = 100, message = "Applicant Id is required and size should not exceed 100 characters.")
	private String applicantId;

	@NotNull(message = "Voyage Id should not be null.")
	private Long voyageId;

	@Size(max = 40, message = "Contact Person size should not exceed 40 characters.")
	private String contactPerson;

	private ContactMethod contactMethod;

	private Integer mobileNumber;

	@NotNull(message = "Expected Time Of Arrival should not be null.")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-11-29 21:20:00")
	private LocalDateTime expectedTimeOfArrival;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-11-29 21:20:00")
	private LocalDateTime reportedTimeOfArrival;

	@Size(max = 100, message = "In Voyage size should not exceed 100 characters.")
	private String inVoyage;

	@Size(max = 100, message = "Out Voyage size should not exceed 100 characters.")
	private String outVoyage;

	@NotNull(message = "Expected Time Of Departure should not be null.")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-11-29 21:20:00")
	private LocalDateTime expectedTimeOfDeparture;

	private OperationStatus operationStatus;

	@Size(max = 5, message = "Location Of Operation size should not exceed 5 characters.")
	private String locationOfOperation;

	@Size(max = 13, message = "Container Number size should not exceed 13 characters.")
	private String containerNumber;

	@Size(max = 17, message = "UCR Number size should not exceed 17 characters.")
	private String ucrNumber;

	@Size(max = 10, message = "IMO Class size should not exceed 10 characters.")
	private String imoClass;

	@Size(max = 4, message = "UN Number size should not exceed 4 characters.")
	private String unNumber;

	private Integer psnCode;

	private Integer technicalNameCode;

	private GoodsPackageType goodsPackageType;

	@Size(max = 250, message = "Package Type Other size should not exceed 250 characters.")
	private String packageTypeOthers;

	private Integer flashPoint;

	private Boolean isoTank = false;

	private Integer numberOfPackages;

	private Integer weight;

	private Boolean hasWaste = false;

	private Boolean hasResidue = false;

	@Size(max = 17, message = "BL Number size should not exceed 17 characters.")
	private String blNumber;

	@Size(max = 10, message = "Subsidiary Risk size should not exceed 10 characters.")
	private String subsidiaryRisk;

	private Boolean tankContainer = false;

	@NotNull(message = "Created On should not be null.")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(example = "2018-11-29 21:20:00")
	private LocalDateTime createdOn;

	@Size(min = 1, max = 100, message = "Created By is required and size should not exceed 100 characters.")
	private String createdBy;

	List<DGDApplicationDocumentEvent> dgdApplicationDocuments = new ArrayList<>();

	@Override
	public String getRoutingKey() {
		return "";
	}
}
