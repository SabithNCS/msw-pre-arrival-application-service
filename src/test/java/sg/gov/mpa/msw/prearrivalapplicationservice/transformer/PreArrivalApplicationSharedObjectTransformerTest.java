package sg.gov.mpa.msw.prearrivalapplicationservice.transformer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.PreArrivalApplication;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.sharedobject.PreArrivalApplicationSharedObject;

@RunWith(MockitoJUnitRunner.class)
public class PreArrivalApplicationSharedObjectTransformerTest {

	@InjectMocks
	PreArrivalApplicationSharedObjectTransformer preArrivalApplicationSharedObjectTransformer;

	@Test
	public void givenValidApplicationTypeWhenTransformThenSuccess() {
		PreArrivalApplication preArrivalApplication = preArrivalApplicationSharedObjectTransformer
				.apply(getSamplePreArrivalApplicationSharedObject());
		assertNotNull(preArrivalApplication);
		assertEquals("1_PRE_ARRIVAL", preArrivalApplication.getId());
		assertTrue(preArrivalApplication.getLockVersion() == 1L);
	}

	private PreArrivalApplicationSharedObject getSamplePreArrivalApplicationSharedObject() {
		PreArrivalApplicationSharedObject preArrivalApplicationSharedObject = new PreArrivalApplicationSharedObject();
		preArrivalApplicationSharedObject.setId("1_PRE_ARRIVAL");
		preArrivalApplicationSharedObject.setLockVersion(1L);
		return preArrivalApplicationSharedObject;
	}
}
