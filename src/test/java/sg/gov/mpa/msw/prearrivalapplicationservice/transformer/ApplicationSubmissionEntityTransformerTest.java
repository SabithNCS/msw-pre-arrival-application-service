package sg.gov.mpa.msw.prearrivalapplicationservice.transformer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationSubmission;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationType;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.PreArrivalApplication;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationSubmissionStatus;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.sharedobject.ApplicationSubmissionSharedObject;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationSubmissionEntityTransformerTest {

	private ApplicationSubmissionEntityTransformer applicationSubmissionEntityTransformer = new ApplicationSubmissionEntityTransformer();

	@Test
	public void givenValidApplicationSubmissionWhenTransformThenSuccess() {
		ApplicationSubmissionSharedObject applicationSubmissionSharedObject = applicationSubmissionEntityTransformer
				.apply(getSampleApplicationSubmissionEntity());
		assertNotNull(applicationSubmissionSharedObject);
		assertEquals("\"{\\\"name\\\":\\\"john\\\",\\\"age\\\":22,\\\"class\\\":\\\"mca\\\"}\"",
				applicationSubmissionSharedObject.getApplicationData());
		assertEquals(ApplicationSubmissionStatus.SUBMITTED,
				applicationSubmissionSharedObject.getApplicationSubmissionStatus());
		assertTrue(applicationSubmissionSharedObject.getId() == 1L);
		assertTrue(applicationSubmissionSharedObject.getLockVersion() == 1L);

	}

	private ApplicationSubmission getSampleApplicationSubmissionEntity() {
		ApplicationSubmission applicationSubmission = new ApplicationSubmission();
		applicationSubmission.setId(1L);
		applicationSubmission
				.setApplicationData("\"{\\\"name\\\":\\\"john\\\",\\\"age\\\":22,\\\"class\\\":\\\"mca\\\"}\"");
		applicationSubmission.setLockVersion(1l);
		applicationSubmission.setApplicationSubmissionStatus(ApplicationSubmissionStatus.SUBMITTED);
		PreArrivalApplication preArrivalApplication = new PreArrivalApplication();
		preArrivalApplication.setId("1_PRE_ARRIVAL");
		preArrivalApplication.setLockVersion(1L);
		ApplicationType applicationType = new ApplicationType();
		applicationType.setId("NOA");
		applicationType.setLockVersion(1L);
		applicationSubmission.setApplicationType(applicationType);
		applicationSubmission.setPreArrivalApplication(preArrivalApplication);
		return applicationSubmission;
	}
}
