package sg.gov.mpa.msw.prearrivalapplicationservice.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.kafka.core.KafkaTemplate;

import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationSubmission;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationType;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.PreArrivalApplication;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationSubmissionStatus;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationTypeName;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.event.PANApplicationSubmittedEvent;
import sg.gov.mpa.msw.prearrivalapplicationservice.repository.ApplicationSubmissionRepository;

@RunWith(MockitoJUnitRunner.class)
public class PreArrivalApplicationServiceTest {

	@Mock
	private ApplicationSubmissionRepository applicationSubmissionRepository;

	@Mock
	private PANApplicationSubmittedEventProducerService panApplicationSubmittedEventProducerService;

	@Mock
	private NOAApplicationSubmittedEventProducerService noaApplicationSubmittedEventProducerService;

	@Mock
	PreArrivalApplicationService preArrivalApplicationService;

	@Mock
	private KafkaTemplate<String, PANApplicationSubmittedEvent> kafkaTemplate;

	@Before
	public void setup() {
		List<ApplicationSubmittedEventProducerService> preArrivalApplicationEventProducerService = Arrays
				.asList(panApplicationSubmittedEventProducerService, noaApplicationSubmittedEventProducerService);
		preArrivalApplicationService = new PreArrivalApplicationService(applicationSubmissionRepository,
				preArrivalApplicationEventProducerService);
	}

	@Test
	public void givenValidPanEventForProduceWhenUpdateThenSuccess() {
		ApplicationSubmission applicationSubmission = getPANApplicationSubmission();
		Throwable throwable = catchThrowable(() -> preArrivalApplicationService.afterUpdate(applicationSubmission));
		assertThat(throwable).isNull();
	}

	@Test
	public void givenValidNoaEventForProduceWhenUpdateThenSuccess() {
		ApplicationSubmission applicationSubmission = getNOAApplicationSubmission();
		Throwable throwable = catchThrowable(() -> preArrivalApplicationService.afterUpdate(applicationSubmission));
		assertThat(throwable).isNull();
	}

	private ApplicationSubmission getPANApplicationSubmission() {
		ApplicationSubmission applicationSubmission = new ApplicationSubmission();
		applicationSubmission.setId(1L);
		applicationSubmission.setApplicationData("{\"voyageId\":12345,\"createdBy\":\"Rohit\"}");
		applicationSubmission.setLockVersion(1l);
		applicationSubmission.setApplicationSubmissionStatus(ApplicationSubmissionStatus.SUBMITTED);
		PreArrivalApplication preArrivalApplication = new PreArrivalApplication();
		preArrivalApplication.setId("1_PRE_ARRIVAL");
		preArrivalApplication.setLockVersion(1L);
		ApplicationType applicationType = new ApplicationType();
		applicationType
				.setId(sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationTypeName.PAN.name());
		applicationType.setLockVersion(1L);
		applicationSubmission.setApplicationType(applicationType);
		applicationSubmission.setPreArrivalApplication(preArrivalApplication);

		return applicationSubmission;
	}

	private ApplicationSubmission getNOAApplicationSubmission() {
		ApplicationSubmission applicationSubmission = new ApplicationSubmission();
		applicationSubmission.setApplicationData(
				"{\"agentEmailAddress\":\"xyz@oohay.com\",\"agentFaxNumber\":\"string\",\"agentName\":\"string\",\"agentPhoneNumber\":\"string\",\"airDraft\":0,\"anchorageDuration\":0,\"applicantId\":\"string\",\"arrivalDraftsAfter\":0,\"arrivalDraftsForward\":0,\"arrivalDraftsMid\":0,\"cargoDescription\":\"string\",\"companySecurityOfficer\":\"string\",\"createdBy\":\"string\",\"createdOn\":\"2018-09-21 21:20:00\",\"csoPhoneNumber\":\"string\", \"deleted\":true,\"externalApplicationReference\":\"string\",\"height\":0,\"inmarsatNumber\":\"string\",\"isSafetyAndSecurityAffected\":true,\"lastUpdatedBy\":\"string\",\"lastUpdatedOn\":\"2018-09-21 21:20:00\",\"lockVersion\":0,\"mswApplicationReferance\":\"string\",\"nameOfPersonReporting\":\"string\",\"panApplicationStatus\":\"NOT_SYNCED\",\"panPurposeOfCall\": [{\"deleted\":true,\"id\":0,\"lockVersion\":0,\"panId\":0,\"purposeOfCall\":\"string\"}],\"securityLevel\":\"NORMAL\",\"securityPersonnelOnboard\":true,\"securityPlanExists\":true,\"shipEmailAddress\":\"ship@oohay.com\",\"slopQuantity\":0,\"slopSludgeContent\":\"string\",\"slopSludgeOnBoard\":true,\"slopSludgeReceptionFacility\":\"string\",\"sludgeQuantity\":0,\"voyageId\":0,\"voyageNumber\":\"string\"}");
		applicationSubmission.setLockVersion(1l);
		applicationSubmission.setApplicationSubmissionStatus(ApplicationSubmissionStatus.SUBMITTED);
		PreArrivalApplication preArrivalApplication = new PreArrivalApplication();
		preArrivalApplication.setId("1_PRE_ARRIVAL");
		preArrivalApplication.setLockVersion(1L);
		ApplicationType applicationType = new ApplicationType();
		applicationType.setId(ApplicationTypeName.NOA.name());
		applicationType.setLockVersion(1L);
		applicationSubmission.setApplicationType(applicationType);
		applicationSubmission.setPreArrivalApplication(preArrivalApplication);
		return applicationSubmission;
	}

}
