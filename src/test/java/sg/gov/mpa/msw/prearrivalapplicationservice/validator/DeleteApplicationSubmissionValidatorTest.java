package sg.gov.mpa.msw.prearrivalapplicationservice.validator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import sg.gov.mpa.msw.core.command.ChangeEntityCommand;
import sg.gov.mpa.msw.core.command.ChangeEntityCommand.ChangeType;
import sg.gov.mpa.msw.prearrivalapplicationservice.exception.ApplicationDeleteException;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationSubmission;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationType;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.PreArrivalApplication;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationSubmissionStatus;
import sg.gov.mpa.msw.prearrivalapplicationservice.repository.ApplicationSubmissionRepository;

@RunWith(MockitoJUnitRunner.class)
public class DeleteApplicationSubmissionValidatorTest {

	@Mock
	ApplicationSubmissionRepository applicationSubmissionRepository;

	@InjectMocks
	DeleteApplicationSubmissionValidator deleteApplicationSubmissionValidator;

	ChangeEntityCommand<ApplicationSubmission, Long> changeEntityCommand;

	@Before
	public void setup() {
		changeEntityCommand = new ChangeEntityCommand<ApplicationSubmission, Long>(ChangeType.DELETE,
				getSampleApplicationSubmissionEntity());
	}

	@Test
	public void givenDeleteRequestForApplicationSubmssionWhenIsApplicableThenSuccess() {
		boolean isApplicable = deleteApplicationSubmissionValidator.isApplicable(changeEntityCommand);

		assertTrue(isApplicable);
	}

	@Test
	public void givenUpdateRequestForApplicationSubmissionWhenIsApplicableThenFailure() {
		changeEntityCommand = new ChangeEntityCommand<ApplicationSubmission, Long>(ChangeType.UPDATE,
				getSampleApplicationSubmissionEntity());
		boolean isApplicable = deleteApplicationSubmissionValidator.isApplicable(changeEntityCommand);

		assertFalse(isApplicable);
	}
	
	
	@Test
	public void givenDeleteRequestWithIdThatExistsInDatabaseInDraftStatusWhenValidateThenSuccess() {
		ApplicationSubmission applicationSubmission = getSampleApplicationSubmissionEntity();
		applicationSubmission.setId(1L);
		applicationSubmission.setApplicationSubmissionStatus(ApplicationSubmissionStatus.DRAFT);
		changeEntityCommand = new ChangeEntityCommand<ApplicationSubmission, Long>(ChangeType.DELETE,
				applicationSubmission);
		when(applicationSubmissionRepository.findById(1L)).thenReturn(Optional.of(applicationSubmission));
		Throwable throwable = catchThrowable(() -> deleteApplicationSubmissionValidator.validate(changeEntityCommand));

		assertThat(throwable).isNull();
	}

	@Test
	public void givenDeleteRequestWithIdThatExistsInDatabaseInSubmittedStatusWhenValidateThenFailure() {
		ApplicationSubmission applicationSubmission = getSampleApplicationSubmissionEntity();
		applicationSubmission.setId(1L);
		changeEntityCommand = new ChangeEntityCommand<ApplicationSubmission, Long>(ChangeType.DELETE,
				applicationSubmission);
		when(applicationSubmissionRepository.findById(1L)).thenReturn(Optional.of(applicationSubmission));
		Throwable throwable = catchThrowable(() -> deleteApplicationSubmissionValidator.validate(changeEntityCommand));

		assertThat(throwable).isInstanceOf(ApplicationDeleteException.class)
				.hasMessageContaining("Active application in process cannot be deleted");
	}

	private ApplicationSubmission getSampleApplicationSubmissionEntity() {
		ApplicationSubmission applicationSubmission = new ApplicationSubmission();
		applicationSubmission
				.setApplicationData("\"{\\\"name\\\":\\\"john\\\",\\\"age\\\":22,\\\"class\\\":\\\"mca\\\"}\"");
		applicationSubmission.setLockVersion(1l);
		applicationSubmission.setApplicationSubmissionStatus(ApplicationSubmissionStatus.SUBMITTED);
		PreArrivalApplication preArrivalApplication = new PreArrivalApplication();
		preArrivalApplication.setId("1_PRE_ARRIVAL");
		preArrivalApplication.setLockVersion(1L);
		ApplicationType applicationType = new ApplicationType();
		applicationType.setId("NOA");
		applicationType.setLockVersion(1L);
		applicationSubmission.setApplicationType(applicationType);
		applicationSubmission.setPreArrivalApplication(preArrivalApplication);
		return applicationSubmission;
	}

}
