package sg.gov.mpa.msw.prearrivalapplicationservice.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import sg.gov.mpa.msw.core.command.ChangeEntityCommand;
import sg.gov.mpa.msw.core.command.dispatcher.SynchronousCommandDispatcher;
import sg.gov.mpa.msw.core.service.DeleteEntityService;
import sg.gov.mpa.msw.core.service.TransformerService;
import sg.gov.mpa.msw.prearrivalapplicationservice.exception.ApplicationDeleteException;
import sg.gov.mpa.msw.prearrivalapplicationservice.exception.ApplicationUpdateException;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationSubmission;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationType;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.PreArrivalApplication;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationSubmissionStatus;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.sharedobject.ApplicationSubmissionSharedObject;
import sg.gov.mpa.msw.prearrivalapplicationservice.repository.ApplicationSubmissionRepository;
import sg.gov.mpa.msw.prearrivalapplicationservice.transformer.ApplicationSubmissionEntityTransformer;
import sg.gov.mpa.msw.prearrivalapplicationservice.transformer.ApplicationSubmissionSharedObjectTransformer;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationSubmissionCommandControllerTest {

	@Mock
	private TransformerService<ApplicationSubmissionSharedObject, ApplicationSubmission, Long> transformerService;

	@Mock
	private SynchronousCommandDispatcher<ChangeEntityCommand<ApplicationSubmission, Long>, ApplicationSubmission> commandDispatcher;

	@Mock
	private DeleteEntityService deleteEntityService;

	@Mock
	private ApplicationSubmissionRepository applicationSubmissionRepository;

	@Mock
	private ApplicationSubmissionSharedObjectTransformer applicationSubmissionSharedObjectTransformer;

	@Mock
	private ApplicationSubmissionEntityTransformer applicationSubmissionEntityTransformer;

	@Mock
	MockHttpServletRequest httpServletRequest;

	@InjectMocks
	ApplicationSubmissionCommandController applicationSubmissionCommandController;

	@Before
	public void setup() {
		applicationSubmissionCommandController.afterPropertiesSet();
		setupHttpRequestMock(httpServletRequest);
		setupTransformerServices();
	}

	private void setupHttpRequestMock(final MockHttpServletRequest httpServletRequestMock) {
		final ServletRequestAttributes servletRequestAttributes = new ServletRequestAttributes(httpServletRequestMock);
		RequestContextHolder.setRequestAttributes(servletRequestAttributes);
	}

	private void setupTransformerServices() {
		when(transformerService.getEntityTransformer(ApplicationSubmissionSharedObject.class,
				ApplicationSubmission.class)).thenReturn(applicationSubmissionSharedObjectTransformer);

		when(transformerService.getDtoTransformer(ApplicationSubmissionSharedObject.class, ApplicationSubmission.class))
				.thenReturn(applicationSubmissionEntityTransformer);
	}

	@Test
	public void whenSubmitNoaWithValidDataThenSuccess() {
		ApplicationSubmissionSharedObject applicationSubmissionsharedObject = getSampleApplicationSubmissionSharedObject();
		ApplicationSubmission entity = getSampleApplicationSubmissionEntity();
		when(commandDispatcher.dispatchAndWaitForResponse(any())).thenReturn(entity);
		when(applicationSubmissionSharedObjectTransformer.apply(any())).thenReturn(entity);
		when(applicationSubmissionEntityTransformer.apply(any())).thenReturn(applicationSubmissionsharedObject);
		ResponseEntity<ApplicationSubmissionSharedObject> responseEntity = applicationSubmissionCommandController
				.create(applicationSubmissionsharedObject, httpServletRequest);
		assertNotNull(responseEntity);
		ApplicationSubmissionSharedObject applicationSubmissionSharedObj = responseEntity.getBody();
		assertNotNull(applicationSubmissionSharedObj);
		assertTrue(applicationSubmissionSharedObj.getId() == 1L);
		assertEquals("NOA", applicationSubmissionSharedObj.getApplicationType());
		assertEquals(ApplicationSubmissionStatus.SUBMITTED,
				applicationSubmissionSharedObj.getApplicationSubmissionStatus());

	}

	@Test
	public void whenSaveNoaWithValidDataThenSuccess() {
		ApplicationSubmissionSharedObject applicationSubmissionSharedObject = getSampleApplicationSubmissionSharedObject();
		applicationSubmissionSharedObject.setApplicationSubmissionStatus(ApplicationSubmissionStatus.DRAFT);
		ApplicationSubmission entity = getSampleApplicationSubmissionEntity();
		entity.setApplicationSubmissionStatus(ApplicationSubmissionStatus.DRAFT);
		when(commandDispatcher.dispatchAndWaitForResponse(any())).thenReturn(entity);
		when(applicationSubmissionSharedObjectTransformer.apply(any())).thenReturn(entity);
		when(applicationSubmissionEntityTransformer.apply(any())).thenReturn(applicationSubmissionSharedObject);
		ResponseEntity<ApplicationSubmissionSharedObject> responseEntity = applicationSubmissionCommandController
				.create(applicationSubmissionSharedObject, httpServletRequest);
		assertNotNull(responseEntity);
		ApplicationSubmissionSharedObject applicationSubmissionSharedObj = responseEntity.getBody();
		assertNotNull(applicationSubmissionSharedObj);
		assertTrue(applicationSubmissionSharedObj.getId() == 1L);
		assertEquals("NOA", applicationSubmissionSharedObj.getApplicationType());
		assertEquals(ApplicationSubmissionStatus.DRAFT,
				applicationSubmissionSharedObj.getApplicationSubmissionStatus());
	}

	@Test
	public void whenDeleteNoaWithIsDraftStatusThenSuccess() {
		ApplicationSubmissionSharedObject applicationSubmissionSharedObject = getSampleApplicationSubmissionSharedObject();
		doNothing().when(deleteEntityService).deleteEntity(any(), any());
		ResponseEntity<Void> testEntityDtoResponseEntity = applicationSubmissionCommandController
				.delete(applicationSubmissionSharedObject.getId());
		assertNotNull(testEntityDtoResponseEntity);
		assertEquals(HttpStatus.NO_CONTENT, testEntityDtoResponseEntity.getStatusCode());
	}

	@Test(expected = ApplicationDeleteException.class)
	public void whenDeleteNoaWithSubmittedStatusThenFailure() {
		ApplicationSubmissionSharedObject applicationSubmissionSharedObject = getSampleApplicationSubmissionSharedObject();
		doThrow(new ApplicationDeleteException("Active application in process cannot be deleted"))
				.when(deleteEntityService).deleteEntity(any(), any());
		applicationSubmissionCommandController.delete(applicationSubmissionSharedObject.getId());
	}

	@Test(expected = ApplicationUpdateException.class)
	public void whenUpdateNoaWithSubmittedStatusThenFailure() {
		ApplicationSubmissionSharedObject applicationSubmissionsharedObject = getSampleApplicationSubmissionSharedObject();
		applicationSubmissionsharedObject.setCreatedBy("XXX");
		ApplicationSubmission entity = getSampleApplicationSubmissionEntity();
		entity.setCreatedBy("XXX");
		doThrow(new ApplicationUpdateException("Active application in process cannot be updated"))
				.when(commandDispatcher).dispatchAndWaitForResponse(any());
		when(applicationSubmissionSharedObjectTransformer.apply(any())).thenReturn(entity);
		applicationSubmissionCommandController.create(applicationSubmissionsharedObject, httpServletRequest);
	}

	@Test
	public void whenUpdateNoaWithIsDraftStatusThenSuccess() {
		ApplicationSubmissionSharedObject applicationSubmissionsharedObject = getSampleApplicationSubmissionSharedObject();
		applicationSubmissionsharedObject.setApplicationSubmissionStatus(ApplicationSubmissionStatus.DRAFT);
		applicationSubmissionsharedObject.setCreatedBy("xxx");
		ApplicationSubmission entity = getSampleApplicationSubmissionEntity();
		entity.setApplicationSubmissionStatus(ApplicationSubmissionStatus.DRAFT);
		entity.setCreatedBy("xxx");
		when(commandDispatcher.dispatchAndWaitForResponse(any())).thenReturn(entity);
		when(applicationSubmissionSharedObjectTransformer.apply(any())).thenReturn(entity);
		when(applicationSubmissionEntityTransformer.apply(any())).thenReturn(applicationSubmissionsharedObject);
		ResponseEntity<ApplicationSubmissionSharedObject> responseEntity = applicationSubmissionCommandController
				.update(applicationSubmissionsharedObject.getId(), applicationSubmissionsharedObject);
		assertNotNull(responseEntity);
		ApplicationSubmissionSharedObject applicationSubmissionSharedObj = responseEntity.getBody();
		assertNotNull(applicationSubmissionSharedObj);
		assertTrue(applicationSubmissionSharedObj.getId() == 1L);
		assertEquals("NOA", applicationSubmissionSharedObj.getApplicationType());
		assertEquals(ApplicationSubmissionStatus.DRAFT,
				applicationSubmissionSharedObj.getApplicationSubmissionStatus());
	}

	private ApplicationSubmission getSampleApplicationSubmissionEntity() {
		ApplicationSubmission applicationSubmission = new ApplicationSubmission();
		applicationSubmission.setId(1L);
		applicationSubmission
				.setApplicationData("\"{\\\"name\\\":\\\"john\\\",\\\"age\\\":22,\\\"class\\\":\\\"mca\\\"}\"");
		applicationSubmission.setLockVersion(1l);
		applicationSubmission.setApplicationSubmissionStatus(ApplicationSubmissionStatus.SUBMITTED);
		PreArrivalApplication preArrivalApplication = new PreArrivalApplication();
		preArrivalApplication.setId("1_PRE_ARRIVAL");
		preArrivalApplication.setLockVersion(1L);
		ApplicationType applicationType = new ApplicationType();
		applicationType.setId("NOA");
		applicationType.setLockVersion(1L);
		applicationSubmission.setApplicationType(applicationType);
		applicationSubmission.setPreArrivalApplication(preArrivalApplication);
		return applicationSubmission;
	}

	private ApplicationSubmissionSharedObject getSampleApplicationSubmissionSharedObject() {
		ApplicationSubmissionSharedObject applicationSubmissionSharedObject = new ApplicationSubmissionSharedObject();
		applicationSubmissionSharedObject.setId(1L);
		applicationSubmissionSharedObject
				.setApplicationData("\"{\\\"name\\\":\\\"john\\\",\\\"age\\\":22,\\\"class\\\":\\\"mca\\\"}\"");
		applicationSubmissionSharedObject.setLockVersion(1l);
		applicationSubmissionSharedObject.setApplicationSubmissionStatus(ApplicationSubmissionStatus.SUBMITTED);
		applicationSubmissionSharedObject.setApplicationType("NOA");
		applicationSubmissionSharedObject.setPreArrivalApplication("1_PRE_ARRIVAL");
		return applicationSubmissionSharedObject;

	}

}
