package sg.gov.mpa.msw.prearrivalapplicationservice.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.util.ReflectionTestUtils;

import sg.gov.mpa.msw.prearrivalapplicationservice.config.KafkaConfig;
import sg.gov.mpa.msw.prearrivalapplicationservice.exception.EventProducerException;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationSubmission;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationType;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.PreArrivalApplication;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationSubmissionStatus;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationTypeName;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.event.NOAApplicationSubmittedEvent;

@RunWith(MockitoJUnitRunner.class)
public class NOAApplicationSubmittedEventProducerServiceTest {

	private NOAApplicationSubmittedEventProducerService noaApplicationSubmittedEventProducerService;

	@Mock
	KafkaConfig kafkaConfig;

	@Mock
	private KafkaTemplate<String, NOAApplicationSubmittedEvent> noaApplicationSubmittedEventKafkaTemplate;

	@Before
	public void setUp() throws Exception {
		noaApplicationSubmittedEventProducerService = new NOAApplicationSubmittedEventProducerService(kafkaConfig,
				noaApplicationSubmittedEventKafkaTemplate, new MappingJackson2HttpMessageConverter());
		ReflectionTestUtils.setField(noaApplicationSubmittedEventProducerService, "noaSubmissionTopic",
				"noas-submission-topic");
	}

	@Test
	public void givenValidNoaEventForProduceerWhenSendThenSuccess() {
		ApplicationSubmission applicationSubmission = getSampleApplicationSubmissionEntity();
		when(kafkaConfig.getTopicPrefix()).thenReturn("msw-");
		noaApplicationSubmittedEventProducerService.sendApplicationSubmittedEvent(applicationSubmission);
		verify(noaApplicationSubmittedEventKafkaTemplate).send("msw-noas-submission-topic",
				getNoaApplicationSubmittedEvent());
	}

	@Test
	public void givenInValidNoaEventForProduceerWhenSendThenFailure() {
		ApplicationSubmission applicationSubmission = getSampleApplicationSubmissionEntity();
		applicationSubmission.setApplicationData(
				"{\"agentEmailAddress\":xyz@oohay.com,\"agentFaxNumber\":\"string\",\"agentName\":\"string\",\"agentPhoneNumber\":\"string\",\"airDraft\":0,\"anchorageDuration\":0,\"applicantId\":\"string\",\"arrivalDraftsAfter\":0,\"arrivalDraftsForward\":0,\"arrivalDraftsMid\":0,\"cargoDescription\":\"string\",\"companySecurityOfficer\":\"string\",\"createdBy\":\"string\",\"createdOn\":\"2018-09-21 21:20:00\",\"csoPhoneNumber\":\"string\", \"deleted\":true,\"externalApplicationReference\":\"string\",\"inmarsatNumber\":\"string\",\"isSafetyAndSecurityAffected\":true,\"lastUpdatedBy\":\"string\",\"lastUpdatedOn\":\"2018-09-21 21:20:00\",\"lockVersion\":0,\"mswApplicationReferance\":\"string\",\"nameOfPersonReporting\":\"string\",\"panApplicationStatus\":\"NOT_SYNCED\",\"panPurposeOfCall\": [{\"deleted\":true,\"id\":0,\"lockVersion\":0,\"panId\":0,\"purposeOfCall\":\"string\"}],\"securityLevel\":\"NORMAL\",\"securityPersonnelOnboard\":true,\"securityPlanExists\":true,\"shipEmailAddress\":\"ship@oohay.com\",\"slopQuantity\":0,\"slopSludgeContent\":\"string\",\"slopSludgeOnBoard\":true,\"slopSludgeReceptionFacility\":\"string\",\"sludgeQuantity\":0,\"voyageId\":0,\"voyageNumber\":\"string\",\"isNOA\":true}");
		Throwable throwable = catchThrowable(
				() -> noaApplicationSubmittedEventProducerService.sendApplicationSubmittedEvent(applicationSubmission));
		assertThat(throwable).isInstanceOf(EventProducerException.class)
				.hasMessageContaining("Exception while producing NOA event");
	}

	private ApplicationSubmission getSampleApplicationSubmissionEntity() {
		ApplicationSubmission applicationSubmission = new ApplicationSubmission();
		applicationSubmission.setId(1L);
		applicationSubmission.setApplicationData("{\"voyageId\":12345,\"createdBy\":\"Rohit\",\"isNOA\":true}");
		applicationSubmission.setLockVersion(1l);
		applicationSubmission.setApplicationSubmissionStatus(ApplicationSubmissionStatus.SUBMITTED);
		PreArrivalApplication preArrivalApplication = new PreArrivalApplication();
		preArrivalApplication.setId("1_PRE_ARRIVAL");
		preArrivalApplication.setLockVersion(1L);
		ApplicationType applicationType = new ApplicationType();
		applicationType.setId(ApplicationTypeName.NOA.name());
		applicationType.setLockVersion(1L);
		applicationSubmission.setApplicationType(applicationType);
		applicationSubmission.setPreArrivalApplication(preArrivalApplication);
		return applicationSubmission;
	}

	private NOAApplicationSubmittedEvent getNoaApplicationSubmittedEvent() {
		NOAApplicationSubmittedEvent noaApplicationSubmittedEvent = new NOAApplicationSubmittedEvent();
		noaApplicationSubmittedEvent.setVoyageId(12345L);
		noaApplicationSubmittedEvent.setCreatedBy("Rohit");
		noaApplicationSubmittedEvent.setIsNOA(true);
		return noaApplicationSubmittedEvent;
	}

}
