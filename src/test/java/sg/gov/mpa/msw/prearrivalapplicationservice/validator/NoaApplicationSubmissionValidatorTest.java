package sg.gov.mpa.msw.prearrivalapplicationservice.validator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import sg.gov.mpa.msw.core.command.ChangeEntityCommand;
import sg.gov.mpa.msw.core.command.ChangeEntityCommand.ChangeType;
import sg.gov.mpa.msw.prearrivalapplicationservice.exception.NoaValidationException;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationSubmission;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationType;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.PreArrivalApplication;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationSubmissionStatus;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationTypeName;
import sg.gov.mpa.msw.prearrivalapplicationservice.repository.ApplicationSubmissionRepository;

@RunWith(MockitoJUnitRunner.class)
public class NoaApplicationSubmissionValidatorTest {

	@Mock
	ApplicationSubmissionRepository applicationSubmissionRepository;

	NoaApplicationSubmissionValidator noaApplicationSubmissionValidator;

	Validator validator;

	ChangeEntityCommand<ApplicationSubmission, Long> changeEntityCommand;

	@Before
	public void setup() {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
		noaApplicationSubmissionValidator = new NoaApplicationSubmissionValidator(validator,
				new MappingJackson2HttpMessageConverter());
		changeEntityCommand = new ChangeEntityCommand<ApplicationSubmission, Long>(ChangeType.UPDATE,
				getSampleApplicationSubmissionEntity());
	}

	@Test
	public void givenUpdateRequestForNoaApplicationWhenIsApplicableThenSuccess() {
		boolean isApplicable = noaApplicationSubmissionValidator.isApplicable(changeEntityCommand);

		assertTrue(isApplicable);
	}

	@Test
	public void givenUpdateRequestForPanApplicationWhenIsApplicableThenFailure() {
		ApplicationSubmission applicationSubmission = getSampleApplicationSubmissionEntity();
		applicationSubmission.getApplicationType()
				.setId(sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationTypeName.PAN.name());
		changeEntityCommand = new ChangeEntityCommand<ApplicationSubmission, Long>(ChangeType.UPDATE,
				applicationSubmission);
		boolean isApplicable = noaApplicationSubmissionValidator.isApplicable(changeEntityCommand);

		assertFalse(isApplicable);
	}

	@Test
	public void givenUpdateRequestWithEmptyNoaDataInDraftStatusWhenValidateThenFailure() {
		ApplicationSubmission applicationSubmission = getSampleApplicationSubmissionEntity();
		applicationSubmission.setApplicationData("{}");
		applicationSubmission.setApplicationSubmissionStatus(ApplicationSubmissionStatus.DRAFT);
		changeEntityCommand = new ChangeEntityCommand<ApplicationSubmission, Long>(ChangeType.UPDATE,
				applicationSubmission);
		Throwable throwable = catchThrowable(() -> noaApplicationSubmissionValidator.validate(changeEntityCommand));

		assertThat(throwable).isInstanceOf(NoaValidationException.class)
				.hasMessageContaining("NOA Application must have atleast one field");
	}

	@Test
	public void givenUpdateRequestWithSingleFieldDataInDraftStatusWhenValidateThenSuccess() {
		ApplicationSubmission applicationSubmission = getSampleApplicationSubmissionEntity();
		applicationSubmission.setApplicationSubmissionStatus(ApplicationSubmissionStatus.DRAFT);
		applicationSubmission.setApplicationData("{\"height\":0}");
		changeEntityCommand = new ChangeEntityCommand<ApplicationSubmission, Long>(ChangeType.UPDATE,
				applicationSubmission);
		Throwable throwable = catchThrowable(() -> noaApplicationSubmissionValidator.validate(changeEntityCommand));

		assertThat(throwable).isNull();
	}

	@Test
	public void givenUpdateRequestWithMissingRequiredFieldInNoaDataInSubmittedStatusWhenValidateThenFailure() {
		ApplicationSubmission applicationSubmission = getSampleApplicationSubmissionEntity();
		applicationSubmission.setApplicationData(
				"{\"agentEmailAddress\":\"xyz@oohay.com\",\"agentFaxNumber\":\"string\",\"agentName\":\"string\",\"agentPhoneNumber\":\"string\",\"airDraft\":0,\"anchorageDuration\":0,\"applicantId\":\"string\",\"arrivalDraftsAfter\":0,\"arrivalDraftsForward\":0,\"arrivalDraftsMid\":0,\"cargoDescription\":\"string\",\"companySecurityOfficer\":\"string\",\"createdBy\":\"string\",\"createdOn\":\"2018-09-21 21:20:00\",\"csoPhoneNumber\":\"string\", \"deleted\":true,\"externalApplicationReference\":\"string\",\"height\":0,\"inmarsatNumber\":\"string\",\"isSafetyAndSecurityAffected\":true,\"lastUpdatedBy\":\"string\",\"lastUpdatedOn\":\"2018-09-21 21:20:00\",\"lockVersion\":0,\"mswApplicationReferance\":\"string\",\"nameOfPersonReporting\":\"string\",\"panApplicationStatus\":\"NOT_SYNCED\",\"panPurposeOfCall\": [{\"deleted\":true,\"id\":0,\"lockVersion\":0,\"panId\":0,\"purposeOfCall\":\"string\"}],\"securityLevel\":\"NORMAL\",\"securityPersonnelOnboard\":true,\"securityPlanExists\":true,\"shipEmailAddress\":\"ship@oohay.com\",\"slopQuantity\":0,\"slopSludgeContent\":\"string\",\"slopSludgeOnBoard\":true,\"slopSludgeReceptionFacility\":\"string\",\"sludgeQuantity\":0,\"voyageNumber\":\"string\"}");
		changeEntityCommand = new ChangeEntityCommand<ApplicationSubmission, Long>(ChangeType.UPDATE,
				applicationSubmission);
		Throwable throwable = catchThrowable(() -> noaApplicationSubmissionValidator.validate(changeEntityCommand));

		assertThat(throwable).isInstanceOf(NoaValidationException.class)
				.hasMessageContaining("Validation failed for Noa application - [message = voyageId must not be null]");
	}

	@Test
	public void givenUpdateRequestWithAllRequiredFieldInNoaDataInSubmittedStatusWhenValidateThenSuccess() {
		ApplicationSubmission applicationSubmission = getSampleApplicationSubmissionEntity();
		changeEntityCommand = new ChangeEntityCommand<ApplicationSubmission, Long>(ChangeType.UPDATE,
				applicationSubmission);
		Throwable throwable = catchThrowable(() -> noaApplicationSubmissionValidator.validate(changeEntityCommand));

		assertThat(throwable).isNull();
	}

	@Test
	public void givenUpdateRequestWithInvalidNoaDataInSubmittedStatusWhenValidateThenFailure() {
		ApplicationSubmission applicationSubmission = getSampleApplicationSubmissionEntity();
		applicationSubmission.setApplicationData(
				"{\"agentEmailAddress\":xyz@oohay.com,\"agentFaxNumber\":\"string\",\"agentName\":\"string\",\"agentPhoneNumber\":\"string\",\"airDraft\":0,\"anchorageDuration\":0,\"applicantId\":\"string\",\"arrivalDraftsAfter\":0,\"arrivalDraftsForward\":0,\"arrivalDraftsMid\":0,\"cargoDescription\":\"string\",\"companySecurityOfficer\":\"string\",\"createdBy\":\"string\",\"createdOn\":\"2018-09-21 21:20:00\",\"csoPhoneNumber\":\"string\", \"deleted\":true,\"externalApplicationReference\":\"string\",\"inmarsatNumber\":\"string\",\"isSafetyAndSecurityAffected\":true,\"lastUpdatedBy\":\"string\",\"lastUpdatedOn\":\"2018-09-21 21:20:00\",\"lockVersion\":0,\"mswApplicationReferance\":\"string\",\"nameOfPersonReporting\":\"string\",\"panApplicationStatus\":\"NOT_SYNCED\",\"panPurposeOfCall\": [{\"deleted\":true,\"id\":0,\"lockVersion\":0,\"panId\":0,\"purposeOfCall\":\"string\"}],\"securityLevel\":\"NORMAL\",\"securityPersonnelOnboard\":true,\"securityPlanExists\":true,\"shipEmailAddress\":\"ship@oohay.com\",\"slopQuantity\":0,\"slopSludgeContent\":\"string\",\"slopSludgeOnBoard\":true,\"slopSludgeReceptionFacility\":\"string\",\"sludgeQuantity\":0,\"voyageId\":0,\"voyageNumber\":\"string\"}");
		changeEntityCommand = new ChangeEntityCommand<ApplicationSubmission, Long>(ChangeType.UPDATE,
				applicationSubmission);
		Throwable throwable = catchThrowable(() -> noaApplicationSubmissionValidator.validate(changeEntityCommand));

		assertThat(throwable).isInstanceOf(NoaValidationException.class)
				.hasMessageContaining("Validation failed for Noa application");
	}

	private ApplicationSubmission getSampleApplicationSubmissionEntity() {
		ApplicationSubmission applicationSubmission = new ApplicationSubmission();
		applicationSubmission.setApplicationData(
				"{\"agentEmailAddress\":\"xyz@oohay.com\",\"agentFaxNumber\":\"string\",\"agentName\":\"string\",\"agentPhoneNumber\":\"string\",\"airDraft\":0,\"anchorageDuration\":0,\"applicantId\":\"string\",\"arrivalDraftsAfter\":0,\"arrivalDraftsForward\":0,\"arrivalDraftsMid\":0,\"cargoDescription\":\"string\",\"companySecurityOfficer\":\"string\",\"createdBy\":\"string\",\"createdOn\":\"2018-09-21 21:20:00\",\"csoPhoneNumber\":\"string\", \"deleted\":true,\"externalApplicationReference\":\"string\",\"height\":0,\"inmarsatNumber\":\"string\",\"isSafetyAndSecurityAffected\":true,\"lastUpdatedBy\":\"string\",\"lastUpdatedOn\":\"2018-09-21 21:20:00\",\"lockVersion\":0,\"mswApplicationReferance\":\"string\",\"nameOfPersonReporting\":\"string\",\"panApplicationStatus\":\"NOT_SYNCED\",\"panPurposeOfCall\": [{\"deleted\":true,\"id\":0,\"lockVersion\":0,\"panId\":0,\"purposeOfCall\":\"string\"}],\"securityLevel\":\"NORMAL\",\"securityPersonnelOnboard\":true,\"securityPlanExists\":true,\"shipEmailAddress\":\"ship@oohay.com\",\"slopQuantity\":0,\"slopSludgeContent\":\"string\",\"slopSludgeOnBoard\":true,\"slopSludgeReceptionFacility\":\"string\",\"sludgeQuantity\":0,\"voyageId\":0,\"voyageNumber\":\"string\"}");
		applicationSubmission.setLockVersion(1l);
		applicationSubmission.setApplicationSubmissionStatus(ApplicationSubmissionStatus.SUBMITTED);
		PreArrivalApplication preArrivalApplication = new PreArrivalApplication();
		preArrivalApplication.setId("1_PRE_ARRIVAL");
		preArrivalApplication.setLockVersion(1L);
		ApplicationType applicationType = new ApplicationType();
		applicationType.setId(ApplicationTypeName.NOA.name());
		applicationType.setLockVersion(1L);
		applicationSubmission.setApplicationType(applicationType);
		applicationSubmission.setPreArrivalApplication(preArrivalApplication);
		return applicationSubmission;
	}

}
