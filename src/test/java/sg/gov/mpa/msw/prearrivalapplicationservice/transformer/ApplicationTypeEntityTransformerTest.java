package sg.gov.mpa.msw.prearrivalapplicationservice.transformer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationType;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.sharedobject.ApplicationTypeSharedObject;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationTypeEntityTransformerTest {

	@InjectMocks
	ApplicationTypeEntityTransformer applicationTypeEntityTransformer;

	@Test
	public void givenValidApplicationTypeWhenTransformThenSuccess() {
		ApplicationTypeSharedObject applicationTypeSharedObject = applicationTypeEntityTransformer
				.apply(getSampleApplicationTypeEntity());
		assertNotNull(applicationTypeSharedObject);
		assertEquals("NOA", applicationTypeSharedObject.getId());
		assertTrue(applicationTypeSharedObject.getLockVersion() == 1L);

	}

	private ApplicationType getSampleApplicationTypeEntity() {
		ApplicationType applicationType = new ApplicationType();
		applicationType.setId("NOA");
		applicationType.setLockVersion(1L);
		return applicationType;
	}

}
