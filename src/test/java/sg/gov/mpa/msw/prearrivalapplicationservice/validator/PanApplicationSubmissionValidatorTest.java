package sg.gov.mpa.msw.prearrivalapplicationservice.validator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import sg.gov.mpa.msw.core.command.ChangeEntityCommand;
import sg.gov.mpa.msw.core.command.ChangeEntityCommand.ChangeType;
import sg.gov.mpa.msw.prearrivalapplicationservice.exception.PanValidationException;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationSubmission;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationType;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.PreArrivalApplication;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationSubmissionStatus;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationTypeName;
import sg.gov.mpa.msw.prearrivalapplicationservice.repository.ApplicationSubmissionRepository;

@RunWith(MockitoJUnitRunner.class)
public class PanApplicationSubmissionValidatorTest {

	@Mock
	ApplicationSubmissionRepository applicationSubmissionRepository;

	PanApplicationSubmissionValidator panApplicationSubmissionValidator;

	Validator validator;

	ChangeEntityCommand<ApplicationSubmission, Long> changeEntityCommand;

	@Before
	public void setup() {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
		panApplicationSubmissionValidator = new PanApplicationSubmissionValidator(applicationSubmissionRepository,
				validator);
		changeEntityCommand = new ChangeEntityCommand<ApplicationSubmission, Long>(ChangeType.CREATE,
				getSampleApplicationSubmissionEntity());
	}

	@Test
	public void givenUpdateRequestForPanApplicationWhenIsApplicableThenSuccess() {
		ApplicationSubmission applicationSubmission = getSampleApplicationSubmissionEntity();
		changeEntityCommand = new ChangeEntityCommand<ApplicationSubmission, Long>(ChangeType.UPDATE,
				applicationSubmission);
		boolean isApplicable = panApplicationSubmissionValidator.isApplicable(changeEntityCommand);

		assertTrue(isApplicable);
	}

	@Test
	public void givenUpdateRequestForNoaApplicationWhenIsApplicableThenFailure() {
		ApplicationSubmission applicationSubmission = getSampleApplicationSubmissionEntity();
		applicationSubmission.getApplicationType()
				.setId(sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationTypeName.NOA.name());
		changeEntityCommand = new ChangeEntityCommand<ApplicationSubmission, Long>(ChangeType.UPDATE,
				applicationSubmission);
		boolean isApplicable = panApplicationSubmissionValidator.isApplicable(changeEntityCommand);

		assertFalse(isApplicable);
	}

	@Test
	public void givenUpdateRequestWithEmptyPanDataInDraftStatusWhenValidateThenFailure() {
		ApplicationSubmission applicationSubmission = getSampleApplicationSubmissionEntity();
		applicationSubmission.setApplicationData("{}");
		applicationSubmission.setApplicationSubmissionStatus(ApplicationSubmissionStatus.DRAFT);
		changeEntityCommand = new ChangeEntityCommand<ApplicationSubmission, Long>(ChangeType.UPDATE,
				applicationSubmission);
		Throwable throwable = catchThrowable(() -> panApplicationSubmissionValidator.validate(changeEntityCommand));

		assertThat(throwable).isInstanceOf(PanValidationException.class)
				.hasMessageContaining("PAN Application must have atleast one field");
	}

	@Test
	public void givenUpdateRequestWithSingleFieldDataInDraftStatusWhenValidateThenSuccess() {
		ApplicationSubmission applicationSubmission = getSampleApplicationSubmissionEntity();
		applicationSubmission.setApplicationSubmissionStatus(ApplicationSubmissionStatus.DRAFT);
		applicationSubmission.setApplicationData("{\"voyageId\":12345}");
		changeEntityCommand = new ChangeEntityCommand<ApplicationSubmission, Long>(ChangeType.UPDATE,
				applicationSubmission);
		Throwable throwable = catchThrowable(() -> panApplicationSubmissionValidator.validate(changeEntityCommand));

		assertThat(throwable).isNull();
	}

	@Test
	public void givenUpdateRequestWithMissingRequiredFieldInPanDataInSubmittedStatusWhenValidateThenFailure() {
		ApplicationSubmission applicationSubmission = getSampleApplicationSubmissionEntity();
		applicationSubmission.setApplicationData(
				"{\"createdBy\":\"rohit\",\"applicantId\":\"UserId\",\"mswApplicationReferance\":\"MSW Reference Id\",\"arrivalDraftsMid\":14.5,\"arrivalDraftsAfter\":42.5,\"airDraft\":42,\"inmarsatNumber\":\"Some Number\",\"companySecurityOfficer\":\"Officer Name\",\"csoPhoneNumber\":\"Phone Number\",\"agentName\":\"Name of Agent\",\"agentPhoneNumber\":\"Agent's\",\"agentFaxNumber\":\"Agent's\",\"agentEmailAddress\":\"xyz@oohay.com\",\"shipEmailAddress\":\"ship@oohay.com\",\"voyageNumber\":\"Voyage Number as given by applicant\",\"anchorageDuration\":24,\"cargoDescription\":\"Description Of Cargo\",\"securityPlanExists\": true,\"securityLevel\":\"NORMAL\",\"securityPersonnelOnboard\":false,\"panPurposeOfCall\":[{\"purposeOfCall\":\"Purpose Of Call 1\"},{\"purposeOfCall\":\"Purpose Of Call 2\"}],\"panShipActivity\":[{\"sequenceNumber\": 1,\"fromDate\":\"2018-09-21 21:20:00\",\"toDate\":\"2018-09-28 21:20:00\",\"location\":\"Malaysia\",\"latitude\":\"25.48383\",\"longitude\":\"78.3434\",\"direction\":\"NORTH\",\"activity\":\"Discharged Cargo\",\"securityMeasure\":\"All Good\"},{\"sequenceNumber\":2,\"fromDate\":\"2018-09-21 21:20:00\",\"toDate\":\"2018-09-21 21:20:00\",\"location\":\"Indonesia\",\"latitude\":\"38.87383\",\"longitude\":\"87.54734\",\"direction\":\"SOUTH\",\"activity\":\"Discharged Passengers\",\"securityMeasure\":\"No Measures\"}], \"panShipCertificate\":[{\"certificateType\":\"Certificate Type XYZ\",\"issueDate\":\"2018-09-21 21:20:00\",\"dueDate\":\"2018-09-21 21:20:00\",\"issuingAuthority\":\"Issuing Authority XYZ\",\"issuingClass\":\"Issuing Class XYZ\"},{\"certificateType\": \"Certificate Type ABC\",\"issueDate\":\"2018-09-21 21:20:00\",\"dueDate\":\"2018-09-21 21:20:00\",\"issuingAuthority\":\"Issuing Authority ABC\",\"issuingClass\":\"Issuing Class ABC\"}]}");
		changeEntityCommand = new ChangeEntityCommand<ApplicationSubmission, Long>(ChangeType.UPDATE,
				applicationSubmission);
		Throwable throwable = catchThrowable(() -> panApplicationSubmissionValidator.validate(changeEntityCommand));

		assertThat(throwable).isInstanceOf(PanValidationException.class)
				.hasMessageContaining("Validation failed for Pan application - [message = voyageId must not be null]");
	}

	@Test
	public void givenUpdateRequestWithAllRequiredFieldInPanDataInSubmittedStatusWhenValidateThenSuccess() {
		ApplicationSubmission applicationSubmission = getSampleApplicationSubmissionEntity();
		changeEntityCommand = new ChangeEntityCommand<ApplicationSubmission, Long>(ChangeType.UPDATE,
				applicationSubmission);
		Throwable throwable = catchThrowable(() -> panApplicationSubmissionValidator.validate(changeEntityCommand));

		assertThat(throwable).isNull();
	}

	@Test
	public void givenUpdateRequestWithInvalidPanDataThatHasMissedEscapeChararacterInSubmittedStatusWhenValidateThenFailure() {
		ApplicationSubmission applicationSubmission = getSampleApplicationSubmissionEntity();
		applicationSubmission.setApplicationData(
				"{\"createdBy\",\"applicantId\":\"UserId\",\"mswApplicationReferance\":\"MSW Reference Id\",\"arrivalDraftsMid\":14.5,\"arrivalDraftsAfter\":42.5,\"airDraft\":42,\"inmarsatNumber\":\"Some Number\",\"companySecurityOfficer\":\"Officer Name\",\"csoPhoneNumber\":\"Phone Number\",\"agentName\":\"Name of Agent\",\"agentPhoneNumber\":\"Agent's\",\"agentFaxNumber\":\"Agent's\",\"agentEmailAddress\":\"xyz@oohay.com\",\"shipEmailAddress\":\"ship@oohay.com\",\"voyageNumber\":\"Voyage Number as given by applicant\",\"anchorageDuration\":24,\"cargoDescription\":\"Description Of Cargo\",\"securityPlanExists\": true,\"securityLevel\":\"NORMAL\",\"securityPersonnelOnboard\":false,\"panPurposeOfCall\":[{\"purposeOfCall\":\"Purpose Of Call 1\"},{\"purposeOfCall\":\"Purpose Of Call 2\"}],\"panShipActivity\":[{\"sequenceNumber\": 1,\"fromDate\":\"2018-09-21 21:20:00\",\"toDate\":\"2018-09-28 21:20:00\",\"location\":\"Malaysia\",\"latitude\":\"25.48383\",\"longitude\":\"78.3434\",\"direction\":\"NORTH\",\"activity\":\"Discharged Cargo\",\"securityMeasure\":\"All Good\"},{\"sequenceNumber\":2,\"fromDate\":\"2018-09-21 21:20:00\",\"toDate\":\"2018-09-21 21:20:00\",\"location\":\"Indonesia\",\"latitude\":\"38.87383\",\"longitude\":\"87.54734\",\"direction\":\"SOUTH\",\"activity\":\"Discharged Passengers\",\"securityMeasure\":\"No Measures\"}], \"panShipCertificate\":[{\"certificateType\":\"Certificate Type XYZ\",\"issueDate\":\"2018-09-21 21:20:00\",\"dueDate\":\"2018-09-21 21:20:00\",\"issuingAuthority\":\"Issuing Authority XYZ\",\"issuingClass\":\"Issuing Class XYZ\"},{\"certificateType\": \"Certificate Type ABC\",\"issueDate\":\"2018-09-21 21:20:00\",\"dueDate\":\"2018-09-21 21:20:00\",\"issuingAuthority\":\"Issuing Authority ABC\",\"issuingClass\":\"Issuing Class ABC\"}]}");
		changeEntityCommand = new ChangeEntityCommand<ApplicationSubmission, Long>(ChangeType.UPDATE,
				applicationSubmission);
		Throwable throwable = catchThrowable(() -> panApplicationSubmissionValidator.validate(changeEntityCommand));

		assertThat(throwable).isInstanceOf(PanValidationException.class)
				.hasMessageContaining("Validation failed for Pan application");
	}

	private ApplicationSubmission getSampleApplicationSubmissionEntity() {
		ApplicationSubmission applicationSubmission = new ApplicationSubmission();
		applicationSubmission.setApplicationData(
				"{\"voyageId\":12345,\"createdBy\":\"rohit\",\"applicantId\":\"UserId\",\"mswApplicationReferance\":\"MSW Reference Id\",\"arrivalDraftsMid\":14.5,\"arrivalDraftsAfter\":42.5,\"airDraft\":42,\"inmarsatNumber\":\"Some Number\",\"companySecurityOfficer\":\"Officer Name\",\"csoPhoneNumber\":\"Phone Number\",\"agentName\":\"Name of Agent\",\"agentPhoneNumber\":\"Agent's\",\"agentFaxNumber\":\"Agent's\",\"agentEmailAddress\":\"xyz@oohay.com\",\"shipEmailAddress\":\"ship@oohay.com\",\"voyageNumber\":\"Voyage Number as given by applicant\",\"anchorageDuration\":24,\"cargoDescription\":\"Description Of Cargo\",\"securityPlanExists\": true,\"securityLevel\":\"NORMAL\",\"securityPersonnelOnboard\":false,\"panPurposeOfCall\":[{\"purposeOfCall\":\"Purpose Of Call 1\"},{\"purposeOfCall\":\"Purpose Of Call 2\"}],\"panShipActivity\":[{\"sequenceNumber\": 1,\"fromDate\":\"2018-09-21 21:20:00\",\"toDate\":\"2018-09-28 21:20:00\",\"location\":\"Malaysia\",\"latitude\":\"25.48383\",\"longitude\":\"78.3434\",\"direction\":\"NORTH\",\"activity\":\"Discharged Cargo\",\"securityMeasure\":\"All Good\"},{\"sequenceNumber\":2,\"fromDate\":\"2018-09-21 21:20:00\",\"toDate\":\"2018-09-21 21:20:00\",\"location\":\"Indonesia\",\"latitude\":\"38.87383\",\"longitude\":\"87.54734\",\"direction\":\"SOUTH\",\"activity\":\"Discharged Passengers\",\"securityMeasure\":\"No Measures\"}], \"panShipCertificate\":[{\"certificateType\":\"Certificate Type XYZ\",\"issueDate\":\"2018-09-21 21:20:00\",\"dueDate\":\"2018-09-21 21:20:00\",\"issuingAuthority\":\"Issuing Authority XYZ\",\"issuingClass\":\"Issuing Class XYZ\"},{\"certificateType\": \"Certificate Type ABC\",\"issueDate\":\"2018-09-21 21:20:00\",\"dueDate\":\"2018-09-21 21:20:00\",\"issuingAuthority\":\"Issuing Authority ABC\",\"issuingClass\":\"Issuing Class ABC\"}]}");
		applicationSubmission.setLockVersion(1l);
		applicationSubmission.setApplicationSubmissionStatus(ApplicationSubmissionStatus.SUBMITTED);
		PreArrivalApplication preArrivalApplication = new PreArrivalApplication();
		preArrivalApplication.setId("1_PRE_ARRIVAL");
		preArrivalApplication.setLockVersion(1L);
		ApplicationType applicationType = new ApplicationType();
		applicationType.setId(ApplicationTypeName.PAN.name());
		applicationType.setLockVersion(1L);
		applicationSubmission.setApplicationType(applicationType);
		applicationSubmission.setPreArrivalApplication(preArrivalApplication);
		return applicationSubmission;
	}

}
