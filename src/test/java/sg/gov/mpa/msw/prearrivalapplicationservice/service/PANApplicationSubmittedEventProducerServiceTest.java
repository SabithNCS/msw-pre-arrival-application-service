package sg.gov.mpa.msw.prearrivalapplicationservice.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.util.ReflectionTestUtils;

import sg.gov.mpa.msw.prearrivalapplicationservice.config.KafkaConfig;
import sg.gov.mpa.msw.prearrivalapplicationservice.exception.EventProducerException;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationSubmission;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationType;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.PreArrivalApplication;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationSubmissionStatus;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationTypeName;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.event.PANApplicationSubmittedEvent;

@RunWith(MockitoJUnitRunner.class)
public class PANApplicationSubmittedEventProducerServiceTest {

	private PANApplicationSubmittedEventProducerService panApplicationSubmittedEventProducerService;

	@Mock
	KafkaConfig kafkaConfig;

	@Mock
	private KafkaTemplate<String, PANApplicationSubmittedEvent> panApplicationSubmittedEventKafkaTemplate;

	@Before
	public void setUp() throws Exception {
		panApplicationSubmittedEventProducerService = new PANApplicationSubmittedEventProducerService(kafkaConfig,
				panApplicationSubmittedEventKafkaTemplate, new MappingJackson2HttpMessageConverter());
		ReflectionTestUtils.setField(panApplicationSubmittedEventProducerService, "panSubmissionTopic",
				"pans-submission-topic");
	}

	@Test
	public void givenValidPanEventForProduceerWhenSendThenSuccess() {
		ApplicationSubmission applicationSubmission = getSampleApplicationSubmissionEntity();
		when(kafkaConfig.getTopicPrefix()).thenReturn("msw-");
		panApplicationSubmittedEventProducerService.sendApplicationSubmittedEvent(applicationSubmission);
		verify(panApplicationSubmittedEventKafkaTemplate).send("msw-pans-submission-topic",
				getPANApplicationSubmittedEvent());
	}

	@Test
	public void givenInValidPanEventForProduceerWhenSendThenFailure() {
		ApplicationSubmission applicationSubmission = getSampleApplicationSubmissionEntity();
		applicationSubmission.setApplicationData(
				"{\"createdBy\":rohit,\"applicantId\":\"UserId\",\"mswApplicationReferance\":\"MSW Reference Id\",\"arrivalDraftsMid\":14.5,\"arrivalDraftsAfter\":42.5,\"airDraft\":42,\"inmarsatNumber\":\"Some Number\",\"companySecurityOfficer\":\"Officer Name\",\"csoPhoneNumber\":\"Phone Number\",\"agentName\":\"Name of Agent\",\"agentPhoneNumber\":\"Agent's\",\"agentFaxNumber\":\"Agent's\",\"agentEmailAddress\":\"xyz@oohay.com\",\"shipEmailAddress\":\"ship@oohay.com\",\"voyageNumber\":\"Voyage Number as given by applicant\",\"anchorageDuration\":24,\"cargoDescription\":\"Description Of Cargo\",\"securityPlanExists\": true,\"securityLevel\":\"NORMAL\",\"securityPersonnelOnboard\":false,\"panPurposeOfCall\":[{\"purposeOfCall\":\"Purpose Of Call 1\"},{\"purposeOfCall\":\"Purpose Of Call 2\"}],\"panShipActivity\":[{\"sequenceNumber\": 1,\"fromDate\":\"2018-09-21 21:20:00\",\"toDate\":\"2018-09-28 21:20:00\",\"location\":\"Malaysia\",\"latitude\":\"25.48383\",\"longitude\":\"78.3434\",\"direction\":\"NORTH\",\"activity\":\"Discharged Cargo\",\"securityMeasure\":\"All Good\"},{\"sequenceNumber\":2,\"fromDate\":\"2018-09-21 21:20:00\",\"toDate\":\"2018-09-21 21:20:00\",\"location\":\"Indonesia\",\"latitude\":\"38.87383\",\"longitude\":\"87.54734\",\"direction\":\"SOUTH\",\"activity\":\"Discharged Passengers\",\"securityMeasure\":\"No Measures\"}], \"panShipCertificate\":[{\"certificateType\":\"Certificate Type XYZ\",\"issueDate\":\"2018-09-21 21:20:00\",\"dueDate\":\"2018-09-21 21:20:00\",\"issuingAuthority\":\"Issuing Authority XYZ\",\"issuingClass\":\"Issuing Class XYZ\"},{\"certificateType\": \"Certificate Type ABC\",\"issueDate\":\"2018-09-21 21:20:00\",\"dueDate\":\"2018-09-21 21:20:00\",\"issuingAuthority\":\"Issuing Authority ABC\",\"issuingClass\":\"Issuing Class ABC\"}]}");
		Throwable throwable = catchThrowable(
				() -> panApplicationSubmittedEventProducerService.sendApplicationSubmittedEvent(applicationSubmission));
		assertThat(throwable).isInstanceOf(EventProducerException.class)
				.hasMessageContaining("Exception while producing PAN event");
	}

	private ApplicationSubmission getSampleApplicationSubmissionEntity() {
		ApplicationSubmission applicationSubmission = new ApplicationSubmission();
		applicationSubmission.setId(1L);
		applicationSubmission.setApplicationData("{\"voyageId\":12345,\"createdBy\":\"Rohit\"}");
		applicationSubmission.setLockVersion(1l);
		applicationSubmission.setApplicationSubmissionStatus(ApplicationSubmissionStatus.SUBMITTED);
		PreArrivalApplication preArrivalApplication = new PreArrivalApplication();
		preArrivalApplication.setId("1_PRE_ARRIVAL");
		preArrivalApplication.setLockVersion(1L);
		ApplicationType applicationType = new ApplicationType();
		applicationType.setId(ApplicationTypeName.PAN.name());
		applicationType.setLockVersion(1L);
		applicationSubmission.setApplicationType(applicationType);
		applicationSubmission.setPreArrivalApplication(preArrivalApplication);
		return applicationSubmission;
	}

	private PANApplicationSubmittedEvent getPANApplicationSubmittedEvent() {
		PANApplicationSubmittedEvent panApplicationSubmittedEvent = new PANApplicationSubmittedEvent();
		panApplicationSubmittedEvent.setVoyageId(12345L);
		panApplicationSubmittedEvent.setCreatedBy("Rohit");
		return panApplicationSubmittedEvent;
	}

}
