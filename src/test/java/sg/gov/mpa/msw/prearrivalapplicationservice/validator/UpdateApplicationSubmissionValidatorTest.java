package sg.gov.mpa.msw.prearrivalapplicationservice.validator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import sg.gov.mpa.msw.core.command.ChangeEntityCommand;
import sg.gov.mpa.msw.core.command.ChangeEntityCommand.ChangeType;
import sg.gov.mpa.msw.prearrivalapplicationservice.exception.ApplicationUpdateException;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationSubmission;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationType;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.PreArrivalApplication;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationSubmissionStatus;
import sg.gov.mpa.msw.prearrivalapplicationservice.repository.ApplicationSubmissionRepository;

@RunWith(MockitoJUnitRunner.class)
public class UpdateApplicationSubmissionValidatorTest {

	@Mock
	ApplicationSubmissionRepository applicationSubmissionRepository;

	@InjectMocks
	UpdateApplicationSubmissionValidator updateApplicationSubmissionValidator;

	ChangeEntityCommand<ApplicationSubmission, Long> changeEntityCommand;

	@Before
	public void setup() {
		changeEntityCommand = new ChangeEntityCommand<ApplicationSubmission, Long>(ChangeType.UPDATE,
				getSampleApplicationSubmissionEntity());
	}

	@Test
	public void givenUpdateRequestForApplicationSubmissionWhenIsApplicableThenSuccess() {
		boolean isApplicable = updateApplicationSubmissionValidator.isApplicable(changeEntityCommand);

		assertTrue(isApplicable);
	}

	@Test
	public void givenDeleteRequestForApplicationSubmissionWhenIsApplicableThenFailure() {
		changeEntityCommand = new ChangeEntityCommand<ApplicationSubmission, Long>(ChangeType.DELETE,
				getSampleApplicationSubmissionEntity());
		boolean isApplicable = updateApplicationSubmissionValidator.isApplicable(changeEntityCommand);

		assertFalse(isApplicable);
	}

	@Test
	public void givenUpdateRequestWithIdThatExistsInDatabaseInDraftStatusWhenValidateThenSuccess() {
		ApplicationSubmission applicationSubmission = getSampleApplicationSubmissionEntity();
		applicationSubmission.setId(1L);
		applicationSubmission.setApplicationSubmissionStatus(ApplicationSubmissionStatus.DRAFT);
		changeEntityCommand = new ChangeEntityCommand<ApplicationSubmission, Long>(ChangeType.UPDATE,
				applicationSubmission);
		when(applicationSubmissionRepository.findById(1L)).thenReturn(Optional.of(applicationSubmission));
		Throwable throwable = catchThrowable(() -> updateApplicationSubmissionValidator.validate(changeEntityCommand));

		assertThat(throwable).isNull();
	}

	@Test
	public void givenUpdateRequestWithIdThatExistsInDatabaseInSubmittedStatusWhenValidateThenFailure() {
		ApplicationSubmission applicationSubmission = getSampleApplicationSubmissionEntity();
		applicationSubmission.setId(1L);
		changeEntityCommand = new ChangeEntityCommand<ApplicationSubmission, Long>(ChangeType.UPDATE,
				applicationSubmission);
		when(applicationSubmissionRepository.findById(1L)).thenReturn(Optional.of(applicationSubmission));
		Throwable throwable = catchThrowable(() -> updateApplicationSubmissionValidator.validate(changeEntityCommand));

		assertThat(throwable).isInstanceOf(ApplicationUpdateException.class)
				.hasMessageContaining("Active application in process cannot be updated");
	}

	private ApplicationSubmission getSampleApplicationSubmissionEntity() {
		ApplicationSubmission applicationSubmission = new ApplicationSubmission();
		applicationSubmission
				.setApplicationData("\"{\\\"name\\\":\\\"john\\\",\\\"age\\\":22,\\\"class\\\":\\\"mca\\\"}\"");
		applicationSubmission.setLockVersion(1l);
		applicationSubmission.setApplicationSubmissionStatus(ApplicationSubmissionStatus.SUBMITTED);
		PreArrivalApplication preArrivalApplication = new PreArrivalApplication();
		preArrivalApplication.setId("1_PRE_ARRIVAL");
		preArrivalApplication.setLockVersion(1L);
		ApplicationType applicationType = new ApplicationType();
		applicationType.setId("NOA");
		applicationType.setLockVersion(1L);
		applicationSubmission.setApplicationType(applicationType);
		applicationSubmission.setPreArrivalApplication(preArrivalApplication);
		return applicationSubmission;
	}

}
