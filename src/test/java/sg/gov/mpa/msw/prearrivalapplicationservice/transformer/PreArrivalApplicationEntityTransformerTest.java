package sg.gov.mpa.msw.prearrivalapplicationservice.transformer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.PreArrivalApplication;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.sharedobject.PreArrivalApplicationSharedObject;

@RunWith(MockitoJUnitRunner.class)
public class PreArrivalApplicationEntityTransformerTest {

	@InjectMocks
	PreArrivalApplicationEntityTransformer preArrivalApplicationEntityTransformer;

	@Test
	public void givenValidApplicationTypeWhenTransformThenSuccess() {
		PreArrivalApplicationSharedObject preArrivalApplicationSharedObject = preArrivalApplicationEntityTransformer
				.apply(getSamplePreArrivalApplicationEntity());
		assertNotNull(preArrivalApplicationSharedObject);
		assertEquals("1_PRE_ARRIVAL", preArrivalApplicationSharedObject.getId());
		assertTrue(preArrivalApplicationSharedObject.getLockVersion() == 1L);

	}

	private PreArrivalApplication getSamplePreArrivalApplicationEntity() {
		PreArrivalApplication preArrivalApplication = new PreArrivalApplication();
		preArrivalApplication.setId("1_PRE_ARRIVAL");
		preArrivalApplication.setLockVersion(1L);
		return preArrivalApplication;
	}
}
