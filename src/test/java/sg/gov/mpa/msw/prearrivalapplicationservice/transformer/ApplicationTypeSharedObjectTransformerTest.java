package sg.gov.mpa.msw.prearrivalapplicationservice.transformer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationType;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.sharedobject.ApplicationTypeSharedObject;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationTypeSharedObjectTransformerTest {

	@InjectMocks
	ApplicationTypeSharedObjectTransformer applicationTypeSharedObjectTransformer;

	@Test
	public void givenValidApplicationTypeWhenTransformThenSuccess() {
		ApplicationType applicationType = applicationTypeSharedObjectTransformer
				.apply(getSampleApplicationTypeSharedObject());
		assertNotNull(applicationType);
		assertEquals("NOA", applicationType.getId());
		assertTrue(applicationType.getLockVersion() == 1L);
	}

	private ApplicationTypeSharedObject getSampleApplicationTypeSharedObject() {
		ApplicationTypeSharedObject applicationTypeSharedObject = new ApplicationTypeSharedObject();
		applicationTypeSharedObject.setId("NOA");
		applicationTypeSharedObject.setLockVersion(1L);
		return applicationTypeSharedObject;
	}
}
