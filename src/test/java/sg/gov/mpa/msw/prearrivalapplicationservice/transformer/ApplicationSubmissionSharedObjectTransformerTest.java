package sg.gov.mpa.msw.prearrivalapplicationservice.transformer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationSubmission;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.ApplicationType;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.entity.PreArrivalApplication;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.enumeration.ApplicationSubmissionStatus;
import sg.gov.mpa.msw.prearrivalapplicationservice.model.sharedobject.ApplicationSubmissionSharedObject;
import sg.gov.mpa.msw.prearrivalapplicationservice.repository.ApplicationTypeRepository;
import sg.gov.mpa.msw.prearrivalapplicationservice.repository.PreArrivalApplicationRepository;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationSubmissionSharedObjectTransformerTest {

	@Mock
	PreArrivalApplicationRepository preArrivalApplicationRepository;

	@Mock
	ApplicationTypeRepository applicationTypeRepository;

	ApplicationSubmissionSharedObjectTransformer applicationSubmissionSharedObjectTransformer;

	@Before
	public void setUp() throws Exception {
		applicationSubmissionSharedObjectTransformer = new ApplicationSubmissionSharedObjectTransformer(
				preArrivalApplicationRepository, applicationTypeRepository);
	}

	@Test
	public void givenPreArrivalApplicationAndVesselTypeIsValidWhenTransformThenSuccess() {

		when(applicationTypeRepository.findById("NOA")).thenReturn(Optional.of(getSampleApplicationTypeEntity()));
		when(preArrivalApplicationRepository.findById("1_PRE_ARRIVAL"))
				.thenReturn(Optional.of(getSamplePreArrivalApplicationEntity()));
		ApplicationSubmissionSharedObject sharedObject = getSampleApplicationSubmissionSharedObject();
		ApplicationSubmission entity = applicationSubmissionSharedObjectTransformer.apply(sharedObject);
		assertNotNull(entity);
		assertTrue(entity.getId() == 1L);
		assertEquals(ApplicationSubmissionStatus.SUBMITTED, entity.getApplicationSubmissionStatus());
		assertEquals("NOA", entity.getApplicationType().getId());
		assertEquals("1_PRE_ARRIVAL", entity.getPreArrivalApplication().getId());
	}

	private ApplicationSubmissionSharedObject getSampleApplicationSubmissionSharedObject() {
		ApplicationSubmissionSharedObject applicationSubmissionSharedObject = new ApplicationSubmissionSharedObject();
		applicationSubmissionSharedObject.setId(1L);
		applicationSubmissionSharedObject
				.setApplicationData("\"{\\\"name\\\":\\\"john\\\",\\\"age\\\":22,\\\"class\\\":\\\"mca\\\"}\"");
		applicationSubmissionSharedObject.setLockVersion(1l);
		applicationSubmissionSharedObject.setApplicationSubmissionStatus(ApplicationSubmissionStatus.SUBMITTED);
		applicationSubmissionSharedObject.setApplicationType("NOA");
		applicationSubmissionSharedObject.setPreArrivalApplication("1_PRE_ARRIVAL");
		return applicationSubmissionSharedObject;

	}

	private PreArrivalApplication getSamplePreArrivalApplicationEntity() {
		PreArrivalApplication preArrivalApplication = new PreArrivalApplication();
		preArrivalApplication.setId("1_PRE_ARRIVAL");
		preArrivalApplication.setLockVersion(1L);
		return preArrivalApplication;
	}

	private ApplicationType getSampleApplicationTypeEntity() {
		ApplicationType applicationType = new ApplicationType();
		applicationType.setId("NOA");
		applicationType.setLockVersion(1L);
		return applicationType;
	}

}
