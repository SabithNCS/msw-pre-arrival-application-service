#!/bin/bash
read -p "Enter name of the service : " serviceName

pattern=" |'"
if [[ $serviceName =~ $pattern ]]
then
	echo "Service Name should not contain spaces or hyphens"
fi

if [ -d "../$serviceName" ]; then
  echo "Directory already exists. Exiting."
  exit
fi


read -p "Enter service title (for example, Notification Service) : " serviceTitle
read -p "Enter service description, one-liner (for example, Notification Service Enterprise API) : " serviceDescription
read -p "Enter service context (will be used for package structure, example sg.gov.mpa.msw.<context>) no spaces, no hyphens: " serviceContext

echo "Generating skeleton for $serviceName..."

mkdir ../$serviceName

cp -a . ../$serviceName/

cd ../$serviceName

echo "Replacing tokens..."
sed -i -e "s/SERVICE_NAME/$serviceName/g" settings.gradle

sed -i -e "s/SERVICE_NAME/$serviceName/g" src/main/resources/application.yml
sed -i -e "s/SERVICE_NAME/$serviceName/g" src/main/resources/bootstrap.yml

sed -i -e "s/SERVICE_TITLE/$serviceTitle/g" src/main/resources/application.yml

sed -i -e "s/SERVICE_DESCRIPTION/$serviceDescription/g" src/main/resources/application.yml
sed -i -e "s/SERVICE_DESCRIPTION/$serviceDescription/g" README.md


sed -i -e "s/BASE_CONTEXT/$serviceContext/g" src/main/java/sg/gov/mpa/msw/BASE_CONTEXT/DemoApplication.java

echo "Replacing DefaultProfileUtil.. "
sed -i -e "s/BASE_CONTEXT/$serviceContext/g" src/main/java/sg/gov/mpa/msw/BASE_CONTEXT/config/DefaultProfileUtil.java

sed -i -e "s/BASE_CONTEXT/$serviceContext/g" src/test/java/sg/gov/mpa/msw/BASE_CONTEXT/DemoApplicationTests.java

mainClassName="$serviceContext"Application

capitalizedClassName="$(tr '[:lower:]' '[:upper:]' <<< ${mainClassName:0:1})${mainClassName:1}"
echo "Main Class Name: " $capitalizedClassName

testClassName="$capitalizedClassName"Tests

mv src/main/java/sg/gov/mpa/msw/BASE_CONTEXT/DemoApplication.java src/main/java/sg/gov/mpa/msw/BASE_CONTEXT/$capitalizedClassName.java
mv src/test/java/sg/gov/mpa/msw/BASE_CONTEXT/DemoApplicationTests.java src/test/java/sg/gov/mpa/msw/BASE_CONTEXT/$testClassName.java

sed -i -e "s/DemoApplication/$capitalizedClassName/g" src/main/java/sg/gov/mpa/msw/BASE_CONTEXT/$capitalizedClassName.java
sed -i -e "s/DemoApplicationTests/$testClassName/g" src/test/java/sg/gov/mpa/msw/BASE_CONTEXT/$testClassName.java

mv src/main/java/sg/gov/mpa/msw/BASE_CONTEXT src/main/java/sg/gov/mpa/msw/$serviceContext
mv src/test/java/sg/gov/mpa/msw/BASE_CONTEXT src/test/java/sg/gov/mpa/msw/$serviceContext